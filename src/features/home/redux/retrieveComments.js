import {
  RETRIEVE_COMMENTS_BEGIN,
  RETRIEVE_COMMENTS_SUCCESS,
  RETRIEVE_COMMENTS_FAILURE,
  FILTER_RETRIEVE_COMMENTS_SUCCESS,
  FILTER_RETRIEVE_COMMENTS_BEGIN,
  FILTER_RETRIEVE_COMMENTS_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveAllComments(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_COMMENTS_BEGIN,
    });
    const url = `${apiEndPoints.comments.RETRIEVE_COMMENTS}?fromDate=${props.fromDate}&toDate=${
      props.toDate
    }&topicId=${props.topicId}&pageLimit=${props.pageLimit}&pageId=${props.currentPage}&sentiment=${
      props.sentiments && props.sentiments.length ? props.sentiments.join(',').toLowerCase() : 'all'
    }&sources=${
      props.sources && props.sources.length ? props.sources.join(',').toLowerCase() : 'all sources'
    }`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_COMMENTS_SUCCESS,
            data:
              res.data.status === 200
                ? { count: res.data.count, data: res.data.data }
                : { count: null, data: [] },
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_COMMENTS_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}
export function filterAllComments(props = {}) {
  return dispatch => {
    dispatch({
      type: FILTER_RETRIEVE_COMMENTS_BEGIN,
    });
    const url = `${apiEndPoints.comments.RETRIEVE_COMMENTS}?fromDate=${props.fromDate}&toDate=${
      props.toDate
    }&topicId=${props.topicId}&pageLimit=${props.pageLimit}&pageId=${props.currentPage}&sentiment=${
      props.sentiments && props.sentiments.length ? props.sentiments.join(',').toLowerCase() : 'all'
    }&sources=${
      props.sources && props.sources.length ? props.sources.join(',').toLowerCase() : 'all sources'
    }`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: FILTER_RETRIEVE_COMMENTS_SUCCESS,
            data:
              res.data.status === 200
                ? { count: res.data.count, data: res.data.data }
                : { count: null, data: [] },
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: FILTER_RETRIEVE_COMMENTS_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_COMMENTS_BEGIN:
      return {
        ...state,
        retrieveCategoriesBegin: true,
        retrieveCategoriesSuccess: false,
        retrieveCategoriesFailure: false,
      };
    case RETRIEVE_COMMENTS_SUCCESS:
      let updatedCommentsData = [];
      if (state.allComments.data && action.data.data) {
        updatedCommentsData = [...state.allComments.data, ...action.data.data];
      } else if (action.data.data) {
        updatedCommentsData = action.data.data;
      }
      return {
        ...state,
        retrieveCategoriesBegin: false,
        retrieveCategoriesSuccess: true,
        allComments: { data: updatedCommentsData, count: action.data.count },
      };
    case RETRIEVE_COMMENTS_FAILURE:
      return {
        ...state,
        retrieveCategoriesBegin: false,
        retrieveCategoriesFailure: true,
      };
    case FILTER_RETRIEVE_COMMENTS_BEGIN:
      return {
        ...state,
        retrieveCategoriesBegin: true,
        retrieveCategoriesSuccess: false,
        allComments: {},
      };
    case FILTER_RETRIEVE_COMMENTS_SUCCESS:
      return {
        ...state,
        retrieveCategoriesBegin: false,
        retrieveCategoriesSuccess: true,
        allComments: { data: action.data.data, count: action.data.count },
      };
    case FILTER_RETRIEVE_COMMENTS_FAILURE:
      return {
        ...state,
        retrieveCategoriesBegin: false,
        retrieveCategoriesFailure: true,
      };
    default:
      return state;
  }
}
