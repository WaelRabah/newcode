import {
  RETRIEVE_TOPICSDROPDOWN_BEGIN,
  RETRIEVE_TOPICSDROPDOWN_SUCCESS,
  RETRIEVE_TOPICSDROPDOWN_FAILURE,
  RETRIEVE_ACTIVE_TOPIC_SUCCESS,
  RETRIEVE_ACTIVE_TOPIC_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';
const topicsDropdownResultLimit = 4;

export function retrieveTopicsDropdown(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_TOPICSDROPDOWN_BEGIN,
    });
    const url = `${apiEndPoints.topicsDropDown.RETRIEVE_TOPICDROPDOWN}?limit=${topicsDropdownResultLimit}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_TOPICSDROPDOWN_SUCCESS,
            data:
              res.data.status === 200
                ? res.data.data.map(topic => {
                    return {
                      ...topic,
                      status: eval(topic.status),
                    };
                  })
                : [],
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_TOPICSDROPDOWN_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function retrieveActiveTopic() {
  return dispatch => {
    const url = `${apiEndPoints.topics.RETRIEVE_TOPICS}?pageLimit=10&pageId=1&status=True`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_ACTIVE_TOPIC_SUCCESS,
            data: res.data.status === 200 ? res.data.data : [],
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_ACTIVE_TOPIC_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_TOPICSDROPDOWN_BEGIN:
      return {
        ...state,
        retrieveTopicsdropdownBegin: true,
        retrieveTopicsdropdownSuccess: false,
        retrieveTopicsdropdownError: false,
      };
    case RETRIEVE_TOPICSDROPDOWN_SUCCESS:
      return {
        ...state,
        retrieveTopicsdropdownBegin: false,
        retrieveTopicsdropdownSuccess: true,
        topicDropdown: action.data,
      };
    case RETRIEVE_TOPICSDROPDOWN_FAILURE:
      return {
        ...state,
        retrieveTopicsdropdownBegin: false,
        retrieveTopicsdropdownError: true,
      };
    case RETRIEVE_ACTIVE_TOPIC_SUCCESS:
      const response = {
        allComments: !action.data.length ? { data: [], count: null } : {},
        allFeeds: !action.data.length ? { data: [], count: null } : {},
        activeTopic: action.data.length ? [action.data[0].topicId] : [],
        headerTopicData: action.data[0],
      };
      return {
        ...state,
        retrieveActiveTopicSucess: true,
        ...response,
      };
    case RETRIEVE_ACTIVE_TOPIC_FAILURE:
      const responseData = {
        allComments: { data: [], count: null },
        allFeeds: { data: [], count: null },
        activeTopic: [],
      };
      return {
        ...state,
        retrieveActiveTopicFailure: true,
        ...responseData,
      };
    default:
      return state;
  }
}
