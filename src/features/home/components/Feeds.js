import React, { useState, useEffect, Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { socialIcons } from '../../../styles/icons';
import ReactGA from 'react-ga'

export class Feeds extends Component {
  static propTypes = {
    feed: PropTypes.object.isRequired,
    handleFeedDetails: PropTypes.func.isRequired,
    updateRelevanceFeed: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      feed: {
        feedDescription: "",
        source: "",
        sentiment: {
          category: ""
        },
        emotion: {
          category: ""
        }
      },
      currentFeed: {
        feedDescription: "",
        source: "",
        sentiment: {
          category: ""
        },
        emotion: {
          category: ""
        }
      },
      updateRelevanceFeed: {},
      isSeeMore: false,
    };
  }

  componentDidMount() {
    this.setState({
      feed: this.props.feed,
      currentFeed: this.props.feed,
      updateRelevanceFeed: this.props.updateRelevanceFeed
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      currentFeed: nextProps.feed,
      feed: nextProps.feed,
      updateRelevanceFeed: nextProps.updateRelevanceFeed
    });
  }


  setRelevance(feedId, currentStatus) {
    const {
      feed,
      currentFeed,
      updateRelevanceFeed
    } = this.state;
    if (feed.relevance !== currentStatus) {
      ReactGA.event(
        {
          category : 'Feeds' ,
          action : 'User with id' + this.props.cookies.userMeta+' set feed with id '+feedId+' as relevant'
        }
      ) 
      currentFeed.relevance = currentStatus;
      updateRelevanceFeed({ feedId, relevance: currentStatus });
    }
    return false;
  }

  numberConverter(number, decPlaces) {
    decPlaces = Math.pow(10, decPlaces);
    var abbrev = ['K', 'M', 'B', 'T'];
    for (var i = abbrev.length - 1; i >= 0; i--) {
      var size = Math.pow(10, (i + 1) * 3);
      if (size <= number) {
        number = Math.round((number * decPlaces) / size) / decPlaces;
        if (number === 1000 && i < abbrev.length - 1) {
          number = 1;
          i++;
        }
        number += abbrev[i];
        break;
      }
    }
    return number;
  }

  toggleSeeMore = isSeeMore => {
    this.setState({isSeeMore});
  }

  _getFeedCSSName = () => {
    const {currentFeed} = this.state;
    return currentFeed.source.toLowerCase().replace(/\s+/g, '_');;
  }

  render() {
    const { 
      currentFeed,
      feed,
      isSeeMore
    } = this.state;
    const feedCSSName = this._getFeedCSSName();
    return (
        <div className="comment-card feeds">
          <div className="feeds-description">
            <div className="feeds-button">
              <div className={`social-button ${feedCSSName}`}>
                <span
                  dangerouslySetInnerHTML={{
                    __html: socialIcons[feedCSSName],
                  }}
                />
              </div>
              {currentFeed.sentiment.length > 0 &&
                currentFeed.sentiment.map((sentiments, index) => (
                  <div className="card-header-button" key={index}>
                    <button
                      className={`btn-${sentiments.category} ${sentiments.category}`}
                      onClick={() => this.handleFeedDetails(sentiments.category)}
                    >
                      {this.numberConverter(sentiments.total, 2)}
                    </button>
                  </div>
                ))}
              <div className="card-header-button">
                {feed.emotion['category'] !== 'no specific emotion' && feed.emotion['category'] && (
                  <div className="card-header-button">
                    <button className={`btn-emotion ${feed.emotion['category']}`}>
                      {feed.emotion['category']}
                    </button>
                  </div>
                )}
              </div>
              <div className="feed-date flex-grow-1">
                <span>{moment(currentFeed.feedDatetime).format('DD MMM YYYY hh:mm A')}</span>
              </div>

              <div className="comment-action-btn">
                <button
                  className={`btn-relevent ${currentFeed.relevance ? 'active' : ''}`}
                  onClick={() => this.setRelevance(currentFeed.feedId, true)}
                >
                  Relevant
                </button>
                <button
                  className={`btn-relevent ${!currentFeed.relevance ? 'active' : ''}`}
                  onClick={() => this.setRelevance(currentFeed.feedId, false)}
                >
                  Irrelevant
                </button>
              </div>
            </div>
            <a href={currentFeed.sourceLink} target="_blank">
              <h4
                dangerouslySetInnerHTML={{ __html: currentFeed.feedTitle }}
                title={currentFeed.feedTitle}
              ></h4>
              <p className={`${!isSeeMore ? 'shrink-text-view' : ''}`}>
                {currentFeed.feedDescription}
              </p>
            </a>
            {currentFeed.feedDescription.length > 135 && !isSeeMore ? (
              <span className="see-more-btn" onClick={() => this.toggleSeeMore(!isSeeMore)}>
                <u>See More</u>
              </span>
            ) : (
              currentFeed.feedDescription.length > 135 &&
              isSeeMore && (
                <span className="see-more-btn less" onClick={() => this.toggleSeeMore(!isSeeMore)}>
                  See Less
                </span>
              )
            )}
          </div>
        </div>
      )
    }
};

export default Feeds;
