import React, { useEffect, useState } from 'react';
import { Modal } from '../../shared-components';
import Feeds from './Feeds';
import FeedComments from './feed-comments';
import { Loader } from '../../shared-components';
import ReactGA from 'react-ga'
const FeedDetails = React.memo(
  ({
    show,
    toggleFeedDetails,
    feed,
    activeFeedCategory,
    feedDetails,
    loadMoreFeedDetails,
    retrieveselectedSentmentFeedDetails,
    updateRelevance,
    updateRelevanceFeed,
  }) => {
    const [responseType, toggleResponseType] = useState('positive');
    const [isLoader, setLoaderActive] = useState(false);
    useEffect(() => {
      document.body.classList.add('modal-open');
      toggleResponseType(activeFeedCategory);
      return () => {
        document.body.classList.remove('modal-open');
      };
    }, [show]);
    const setResponseType = type => {
      if (responseType !== type) {
        toggleResponseType(type);
        retrieveselectedSentmentFeedDetails(type);
      }
    };
    const handleLoadMore = () => {
      setLoaderActive(true);
      loadMoreFeedDetails();
      setTimeout(() => {
        setLoaderActive(false);
      }, 2000);
    };

    function numberConverter(number, decPlaces) {
      decPlaces = Math.pow(10, decPlaces);
      var abbrev = ['K', 'M', 'B', 'T'];
      for (var i = abbrev.length - 1; i >= 0; i--) {
        var size = Math.pow(10, (i + 1) * 3);
        if (size <= number) {
          number = Math.round((number * decPlaces) / size) / decPlaces;
          if (number === 1000 && i < abbrev.length - 1) {
            number = 1;
            i++;
          }
          number += abbrev[i];
          break;
        }
      }
      return number;
    }
    function handleFeedDetails() {
      return;
    }
    return (
      <Modal handleClick={() => toggleFeedDetails(false)}>
        <div className={`modal feed-details-modal ${show ? 'modal-show' : 'modal-hide'}`}>
          {show && (
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h4
                    className="modal-title"
                    dangerouslySetInnerHTML={{ __html: feed.feedTitle }}
                    title={feed.feedTitle}
                  />
                  <i className="la la-close modal-close" onClick={() => toggleFeedDetails(false)} />
                </div>
                <div className="modal-body py-0">
                  <Feeds
                    feed={feed}
                    isFeedModal={true}
                    updateRelevanceFeed={e => updateRelevanceFeed(e)}
                    handleFeedDetails={() => handleFeedDetails()}
                  />
                  <div className="toggle-response">
                    <ul>
                      <li
                        onClick={() => setResponseType('positive')}
                        className={`${responseType === 'positive' ? 'active' : ''}`}
                      >
                        Positive ({numberConverter(feed['sentiment'][0].total, 2)})
                      </li>
                      <li
                        onClick={() => setResponseType('negative')}
                        className={`${responseType === 'negative' ? 'active' : ''}`}
                      >
                        Negative ({numberConverter(feed['sentiment'][1].total, 2)})
                      </li>
                    </ul>
                  </div>
                  <div className="response-slide-in pr-2">
                    <ul>
                      <li>
                        <div className="feed-comments feed-details-container">
                          {!feedDetails['data'] && <Loader />}
                          {feedDetails['data'] &&
                            feedDetails['data'].length > 0 &&
                            feedDetails.data.map((feed, index) => (
                              <FeedComments
                                userComment={feed}
                                key={feed.commentId}
                                updateRelevance={e => updateRelevance(e)}
                              />
                            ))}
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="modal-footer">
                  {feedDetails.count &&
                    feedDetails.data &&
                    feedDetails.count > feedDetails.data.length && (
                      <button
                        className={`btn btn-loadmore ${isLoader ? 'disabled' : ''}`}
                        onClick={handleLoadMore}
                      >
                        {isLoader ? (
                          <React.Fragment>
                            <i className="la la-spinner la-spin" /> Loading
                          </React.Fragment>
                        ) : (
                          'LOAD MORE'
                        )}
                      </button>
                    )}
                </div>
              </div>
            </div>
          )}
        </div>
      </Modal>
    );
  },
);

export default FeedDetails;
