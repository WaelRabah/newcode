import React, { Component } from 'react';
import Filter from './Filter';
import Feeds from './Feeds';
import FeedComments from './feed-comments';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import ReactGA from 'react-ga';
import moment from 'moment';
import {
  Button,
  Calender,
  WorldMapChart,
  WordCloud,
  BarChart,
  PieChart,
  ColumnChart,
  SourcesPercentColumnChart,
  SpiderChart,
  AreaChart,
  LineChart,
  SentimentsChart,
  EmotionsChart,
  NoTopic,
  Loader,
  ContactUs,
} from '../../shared-components';
import FeedDetails from './Feeds-Detail-Modal';
import AddChartModal from './AddChartModal';
import ShareReportModal from './ShareReportModal';
import { Link } from 'react-router-dom';
import qs from 'qs';
const feedMenu = ['comments', 'feeds'];
const commentFilters = [
  { value: 'all', name: 'all', checked: true },
  { value: 'Positive', name: 'positive', checked: true },
  { value: 'negative', name: 'negative', checked: true },
  { value: 'neutral', name: 'neutral', checked: true },
  { value: 'joy', name: 'joy', checked: true },
  { value: 'anger', name: 'anger', checked: true },
  { value: 'fear', name: 'fear', checked: true },
  { value: 'sadness', name: 'sadness', checked: true },
];
const feedModalItemCount = 10;
class Dashboard extends Component {
  constructor(props) {
    super(props);
    const {cookies} = props 
    this.state = {
      activeFeed: 'comments',
      isCommentFilter: false,
      commentFilterItems: JSON.parse(JSON.stringify(commentFilters)),
      isLoader: false,
      isFeedModal: false,
      selectedFeedIndex: null,
      activeFeedCategory: 'positive',
      isAddChartActive: false,
      isCommentsActive: false,
      isShareModalActive: false,
      dateRangeFilterValue: {
        fromDate: moment()
          .subtract(6, 'days')
          .format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      filterValue: '',
      sourcesFilter: [],
      isSendReport: false,
      reportMailinglist: [],
      isFilterInChart: false,
      isContactUs: false,
      comments: {
        'data': [],
        count: 0
      },
      feeds: {
        'data': [],
        count: 0
      }
    };
    document.addEventListener('mousedown', this.handleClickOutside);
    this.wrapperRef = React.createRef();
    this.chartContainer = React.createRef();
  }

  setCommentsRelevant = commentId => {
    const { updateCommentsRelevant } = this.props;
    updateCommentsRelevant(commentId);
  };

  setFeedsRelevant = feedId => {
    const { updateFeedsRelevant } = this.props;
    updateFeedsRelevant(feedId);
  };
  showContactUs = () => {
    this.setState(prevState => {
      return {
        isContactUs: !prevState.isContactUs,
      };
    });
  };
  handleCommentView = type => {
    ReactGA.event(
      {
        category : type ,
        action : `user with id  ${this.props.cookies.cookies.userMeta} is viewing the ${type} `
      }
    ) 
    
    
    
    this.setState(prevState => {
      return {
        activeFeed: type,
        isCommentsActive: !prevState.isCommentsActive,
      };
    });
  };

  updateAvailableChart = e => {
    ReactGA.event(
      {
        category : 'Chart' ,
        action : `user with user id ${this.props.cookies.cookies.userMeta} added a chart ${JSON.stringify(e)}`
      }
    )
    this.props.updateSelectedCharts(e);
    this.setState({ isAddChartActive: false });
  };
  handleClickOutside = event => {
    if (this.wrapperRef.current && !this.wrapperRef.current.contains(event.target)) {
      this.setState({
        isCommentFilter: false,
      });
    }
  };

  filterBySource = source => {
    if (source.length) {
      this.props.commentsFilter({ value: source, key: 'sources', sources: source });
      this.setState({ sourcesFilter: source }, () => {
        let filter = localStorage.getItem('dashboardFilter');
        filter =
          filter && filter.length
            ? { ...JSON.parse(filter), ...{ source: this.state.sourcesFilter } }
            : { source: this.state.sourcesFilter };
        localStorage.setItem('dashboardFilter', JSON.stringify(filter));
        ReactGA.event(
          {
            category : 'Sources_Filter' ,
            action : `user with id ${this.props.cookies.cookies.userMeta} filtered sources in dashboard by ${JSON.stringify(filter)}`
          }
        )
      });
    } else {
      let filter = localStorage.getItem('dashboardFilter');
      if (filter) {
        filter = JSON.parse(filter);
        if (filter.source) {
          delete filter.source;
        }
        localStorage.setItem('dashboardFilter', JSON.stringify(filter));
      }
    }
  };
  updateDimensions = () => {
    if (window.innerWidth <= 1024) {
      this.setState({ isCommentsActive: false });
    } 
  };

  componentDidMount() {
    const { cookies } = this.props;
    ReactGA.pageview('/dashboard/'+cookies.cookies.userMeta)  
    window.addEventListener('resize', this.updateDimensions);
    this.setState({ isCommentsActive: false });
    const parsed = qs.parse(window.location.search,  { ignoreQueryPrefix: true });
    let filter = localStorage.getItem('dashboardFilter');
    if (parsed.fromDate && parsed.toDate) {
      let queryFilter = {
        fromDate: parsed['fromDate'],
        toDate: parsed['toDate']
      };
      this.setDateRangeFilter(queryFilter);
    }
    else if (filter) {
      filter = JSON.parse(filter);
      if (filter.dateRangeFilterValue) {
        this.setState({
          dateRangeFilterValue: filter.dateRangeFilterValue,
        });
      }
    }

    this._loadCommentsFilterFromLocalStorageIntoState();
    this._setSourcesFilterToStateIfNewSourcesFilter(this.props);
    this._setCommentsToStateIfNewComments(this.props);
    this._setFeedsToStateIfNewFeeds(this.props);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
    window.removeEventListener('resize', this.updateDimensions);
  }

  handleFeedDetails = (sentiment, index) => {
    this.props.retrieveFeedDetails({
      sentiment,
      pageLimit: feedModalItemCount,
      pageId: 1,
      feedId: this.props.feeds.data[index].feedId,
    });
    this.setState({
      isFeedModal: true,
      activeFeedCategory: sentiment,
      selectedFeedIndex: index,
      selectedFeedId: this.props.feeds.data[index].feedId,
    });
  };
  retrieveselectedSentmentFeedDetails = sentiment => {
    const { selectedFeedId } = this.state;
    const queryParams = {
      sentiment,
      pageLimit: feedModalItemCount,
      pageId: 1,
      feedId: selectedFeedId,
    };
    this.setState(
      {
        activeFeedCategory: sentiment,
        feedModalCurrentPage: 1,
      },
      () => {
        this.props.retrieveFeedDetails(queryParams);
      },
    );
  };

  loadMoreFeedDetails = () => {
    const { feedModalCurrentPage, selectedFeedId } = this.state;
    const queryParams = {
      sentiment: this.state.activeFeedCategory,
      pageLimit: feedModalItemCount,
      pageId: Number(feedModalCurrentPage) + 1,
      feedId: selectedFeedId,
    };
    this.setState(
      {
        feedModalCurrentPage: Number(feedModalCurrentPage) + 1,
      },
      () => {
        this.props.loadMoreFeedDetails(queryParams);
      },
    );
  };
  handleCommentsFilter = event => {
    const { commentFilterItems } = this.state;
    const value = event.target.value;

    const updatedFilter = commentFilterItems.map(filterItem => {
      if (value === 'all') {
        
          if (event.target.checked)
         {
          ReactGA.event(
            {
              category : 'Comments' ,
              action : 'User with id' + this.props.cookies.cookies.userMeta+' filtered comments according to all filters'
            }
          ) 
         }
         else 
         {
          ReactGA.event(
            {
              category : 'Comments' ,
              action : 'User with id ' + this.props.cookies.cookies.userMeta+' removed the '+value+' filter'
            }
          ) 
         }
        
       
        return {
          ...filterItem,
          checked: event.target.checked,
        };
      }
      
      if (filterItem.value === value)
      {
        if (event.target.checked)
        {
         ReactGA.event(
           {
             category : 'Comments' ,
             action : 'User with id ' + this.props.cookies.cookies.userMeta+' filtered comments according to '+value
           }
         ) 
        } 
        else 
        {
         ReactGA.event(
           {
             category : 'Comments' ,
             action : 'User with id ' + this.props.cookies.cookies.userMeta+' removed the '+value+' filter'
           }
         ) 
        }  
        return {
          ...filterItem,
          checked: !filterItem.checked,
        };
       }
      
      
      return filterItem;
    
    });
    const selectedItems = updatedFilter.filter(filter => filter.checked && filter.value !== 'all');
    if (selectedItems.length === 7) {
      updatedFilter[0].checked = true;
    } else {
      updatedFilter[0].checked = false;
    }
    console.log(updatedFilter)
    this.setState(
      {
        commentFilterItems: updatedFilter,
      },
      () => {
        let filter = '';
        if (value !== 'all') {
          filter = this.state.commentFilterItems.filter(item => item.checked);

          filter = filter.map(i => i.value);
          if (filter.indexOf('all') !== -1) filter.splice(filter.indexOf('all'), 1);
        }
        this.props.commentsFilter({ value: filter, key: 'sentiments' });
        this._saveCommentsFilterFromStateIntoLocalStorage();
      },
    );
  };

  _saveCommentsFilterFromStateIntoLocalStorage() {
    let dashboardFilter = JSON.parse(localStorage.getItem('dashboardFilter'));
    if (! dashboardFilter) {
      dashboardFilter = {};
    }
    dashboardFilter.commentFilterItems = this.state.commentFilterItems;
    localStorage.setItem('dashboardFilter',JSON.stringify(dashboardFilter));
  }

  _loadCommentsFilterFromLocalStorageIntoState() {
    let dashboardFilter = JSON.parse(localStorage.getItem('dashboardFilter'));
    if (dashboardFilter && dashboardFilter.commentFilterItems) {
      this.setState({commentFilterItems: dashboardFilter.commentFilterItems});
    }
  }

  loadMoreAction = () => {
    this.setState({ isLoader: true }, () => {
      setTimeout(() => {
        this.setState({ isLoader: false });
      }, 2000);
    });
  };

  resetChartFilter = () => {
    ReactGA.event(
      {
        category : 'Chart' ,
        action : `user with user id ${this.props.cookies.userMeta} has reset his chart filter`
      }
    )
    this.setState(
      {
        isFilterInChart: false,
      },
      () => {
        this.props.resetFilterInChart();
      },
    );
  };

  filterBySentimentsFromChart = e => {
    this.setState({
      isFilterInChart: true,
    });
    ReactGA.event(
      {
        category : 'Chart' ,
        action : `user with user id ${this.props.cookies.userMeta} filtered his charts by ${e.sentiment}`
      }
    )
    this.props.commentsFilter({ value: e.sentiment, key: 'sentiments', date: e.dateTime });
  };

  exportToPdf = () => {
    const el = this.chartContainer.current;
    const HTML_Width = el.offsetWidth;
    const HTML_Height = el.offsetHeight;
    const top_left_margin = 30;
    const PDF_Width = HTML_Width + top_left_margin * 2;
    const PDF_Height = PDF_Width * 1.5 + top_left_margin * 2;
    const canvas_image_width = HTML_Width;
    const canvas_image_height = HTML_Height;
    const totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
    const currentDate = moment().format('MM-DD-YYYY');
    
    
    let topicName = this.props.headerTopicData.topicName || 'NO_TOPIC';
    ReactGA.event(
      {
        category : 'User' ,
        action : `User with id ${this.props.cookies.cookies.userMeta} downloaded the report for ${topicName} on ${currentDate}  as a pdf file `
      }
    ) 
    html2canvas(el, {
      allowTaint: true,
      removeContainer: false,
    }).then(function(canvas) {
      canvas.getContext('2d');
     
      var imgData = canvas.toDataURL('image/png', 1);
      var pdf = new jsPDF('', 'pt', [PDF_Height, PDF_Width]);
      pdf.setTextColor(102, 102, 102);
      pdf.setFillColor(247, 247, 247, 1);
      pdf.setFontSize(12);
      pdf.text(top_left_margin, 25, `Topic: ${topicName}`);
      pdf.text(canvas_image_width - 130, 25, `Downloaded on: ${currentDate}`);
      pdf.addImage(
        imgData,
        'JPG',
        top_left_margin,
        top_left_margin,
        canvas_image_width,
        canvas_image_height,
      );

      for (var i = 1; i <= totalPDFPages; i++) {
        pdf.addPage(PDF_Width, PDF_Height);
        pdf.addImage(
          imgData,
          'JPG',
          top_left_margin,
          -(PDF_Height * i) + top_left_margin,
          canvas_image_width,
          canvas_image_height,
        );
      }

      pdf.save(`${topicName}_${currentDate}_report.pdf`);
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!nextProps.isShareReportSending) {
      this.setState({ isSendReport: false, isShareModalActive: false });
    }
    this._setSourcesFilterToStateIfNewSourcesFilter(nextProps);
    this._setCommentsToStateIfNewComments(nextProps);
    this._setFeedsToStateIfNewFeeds(nextProps);
  }

  _setSourcesFilterToStateIfNewSourcesFilter(nextProps) {
    if (
      Object.keys(nextProps.sources).length &&
      JSON.stringify(this.state.sourcesFilter) !== JSON.stringify(nextProps.sources)
    ) {
        this.setState({sourcesFilter: nextProps.sources});
    }
  }

  _setCommentsToStateIfNewComments(nextProps) {
    if (
      Object.keys(nextProps.comments).length &&
      JSON.stringify(this.state.comments) !== JSON.stringify(nextProps.comments)
    ) {
      if (Array.isArray(nextProps.comments.data)) {
        this.setState({
          comments: nextProps.comments,
        });
      }
    }
  }

  _setFeedsToStateIfNewFeeds(nextProps) {
    if (
      Object.keys(nextProps.feeds).length &&
      JSON.stringify(this.state.feeds) !== JSON.stringify(nextProps.feeds)
    ) {
      if (Array.isArray(nextProps.feeds.data)) {
        let feeds = {
          'count': nextProps.feeds.count,
          'data': []
        };
        let topicId = nextProps.topicId[0];
        for (let i=0;i<nextProps.feeds.data.length;i++) {
          let feed = nextProps.feeds.data[i];
          if (feed.topicId == topicId) {
            feeds.data.push(feed);
          }
        }
        this.setState({
          feeds: feeds,
        });
      }
    }
  }

  sendReportEmail = () => {
    let topicName   = this.props.headerTopicData.topicName || 'NO_TOPIC';
    const {topicId} = this.props;
    const { 
      reportMailinglist,
      dateRangeFilterValue
    } = this.state;
    ReactGA.event(
      {
        category : 'User' ,
        action : `User with id ${this.props.cookies.cookies.userMeta} shared the report with ${reportMailinglist}`
      }
    ) 
    const formData = {
      email: reportMailinglist.join(','),
      topic_id: topicId[0],
      topic_name: topicName,
      from_date: dateRangeFilterValue.fromDate,
      to_date: dateRangeFilterValue.toDate
    };
    this.props.sendEmailReport(formData);
  };

  sendReport = emailList => {
    this.setState(
      {
        isSendReport: true,
        reportMailinglist: emailList,
      },
      () => {
        this.sendReportEmail();
      },
    );
  };

  setDateRangeFilter = e => {
    this.setState({ dateRangeFilterValue: e }, () => {
      let filter = localStorage.getItem('dashboardFilter');
      filter =
        filter && filter.length
          ? { ...JSON.parse(filter), ...{ dateRangeFilterValue: e } }
          : { dateRangeFilterValue: e };
      localStorage.setItem('dashboardFilter', JSON.stringify(filter));
    });
    this.props.filterFeedsCommentsByDate(e);
  };

  setShareReportModal = () => {
    this.setState({ isShareModalActive: true });
    this.props.retrieveShareReportMessage();
  };

  actionButtons = activeFeed => {
    const { commentFilterItems, isCommentFilter } = this.state;
    const { comments, feeds } = this.props;
    return (
      <div className="feeds-action">
          <Button
            icon="download"
            label="CSV"
            onClick={() => {
              
              ReactGA.event(
                {
                  category : activeFeed ,
                  action : activeFeed + ' csv data was downloaded by user with id '+this.props.cookies.cookies.userMeta
                }
              )    
              this.props.getCsvData(activeFeed);
            }}
          />
          <div
            className={`input-container ${isCommentFilter ? 'active' : ''}`}
            ref={this.wrapperRef}
          >
            <div
              className="toggle-btn"
              onClick={() =>
                this.setState(prevState => {
                  return { isCommentFilter: !prevState.isCommentFilter };
                })
              }
            >
              <i className="la la-filter icon" />
              <input type="text" placeholder="Comments" readOnly />
            </div>
            {isCommentFilter && (
              <div className="comments-filter-item">
                {commentFilterItems.map((commentFilter, index) => (
                  <div className="select-item" key={index}>
                    <input
                      className="styled-checkbox"
                      id={`checkbox-${commentFilter.value}`}
                      type="checkbox"
                      onChange={e => this.handleCommentsFilter(e)}
                      value={commentFilter.value}
                      checked={commentFilter.checked}
                    />
                    <label htmlFor={`checkbox-${commentFilter.value}`}>{commentFilter.name}</label>
                  </div>
                ))}
              </div>
            )}
          </div>
        
      </div>
    );
  };
  render() {
    
    const {
      isFeedModal,
      selectedFeedIndex,
      activeFeedCategory,
      isAddChartActive,
      isCommentsActive,
      activeFeed,
      isShareModalActive,
      dateRangeFilterValue,
      isFilterInChart,
      isContactUs,
      feeds,
      comments,
    } = this.state;
    const {
      availableCharts,
      isCommentsLoading,
      sourcesFilter,
      isFeedsLoading,
      topicId,
      updateSelectedCharts,
      isShareReportSending,
      retrieveCategoriesBegin,
      retrieveFeedsBegin,
      feedDetails,
    } = this.props;

    const updatedSourcesFilter = JSON.parse(JSON.stringify(sourcesFilter)).map(source => {
      source.checked = true;
      return source;
    });
    return (
      <div className="container-fluid">
        <div className="d-flex dashboard-container">
          <div className="home-analytics w-100">
            <div className="analytics-menu">
              {topicId.length > 0 && (
                <React.Fragment>
                  <p>Overview</p>
                  {availableCharts.length > 0 && (
                    <div className="menu-item">
                      {isFilterInChart && (
                        <Button
                          icon="refresh"
                          label="Reset"
                          onClick={() => this.resetChartFilter()}
                        />
                      )}
                      <Button
                        icon="envelope"
                        label="Share"
                        onClick={() => this.setState({ isShareModalActive: true })}
                      />
                      <Button icon="download" label="PDF" onClick={() => this.exportToPdf()} />
                      <Filter
                        sourcesFilter={updatedSourcesFilter}
                        filterBySource={e => this.filterBySource(e)}
                      />
                      <Calender setDateRange={e => this.setDateRangeFilter(e)} />
                    </div>
                  )}
                </React.Fragment>
              )}
            </div>
            <div className="analytics-container" ref={this.chartContainer}>
              <div className="row chart-container">
                {topicId && topicId.length > 0 ? (
                  availableCharts.map(
                    charts =>
                      charts.status && (
                        <React.Fragment key={charts.chartId}>
                          <RenderAvailableChart
                            cookies={this.props.cookies}
                            chart={{ ...charts }}
                            selectedTopicId={topicId && topicId.length ? [...topicId] : []}
                            dateFilterValue={dateRangeFilterValue}
                            updateSelectedCharts={e => updateSelectedCharts(e)}
                            source={this.state.sourcesFilter}
                            filterBySentiments={e => this.filterBySentimentsFromChart(e)}
                          />
                        </React.Fragment>
                      ),
                  )
                ) : (
                  <NoTopic
                    title="Almost There"
                    message="Add topics and analyse the sentiments and emotions here."
                    icons={['area-chart', 'pie-chart', 'bar-chart']}
                  >
                    <Link to="/dashboard/topics">
                      <button className="btn-add-topic">ADD TOPIC</button>
                    </Link>
                  </NoTopic>
                )}
                {topicId && topicId.length > 0 && availableCharts.length > 0 && (
                  <div
                    className="chart-content"
                    data-html2canvas-ignore="true"
                    onClick={() => this.setState({ isAddChartActive: true })}
                  >
                    <div className="add-chart-container">
                      <div className="add-char-inner">
                        <span>
                          <i className="la la-plus" /> Add Chart
                        </span>
                      </div>
                    </div>
                  </div>
                )}
                {topicId && topicId.length > 0 && !availableCharts.length && (
                  <div className="no-feed-comment-data">
                    <NoTopic
                      title="Something’s Missing"
                      message="Please check your Topics and Sources configuration. Otherwise, contact us for assistance."
                      icons={['exclamation-circle']}
                    >
                      <button className="btn-no-data" onClick={() => this.showContactUs()}>
                        CONTACT US
                      </button>
                    </NoTopic>
                  </div>
                )}
              </div>
            </div>
            <div
              className={`toggle-button ${isCommentsActive ? 'active' : ''}`}
              onClick={() =>
                {
                  ReactGA.event(
                    {
                      category : this.state.activeFeed ,
                      action : 'User with id ' + this.props.cookies.cookies.userMeta+' is viewing '+this.state.activeFeed
                    }
                  ) 
                  this.setState(prevState => {
                  return { isCommentsActive: !prevState.isCommentsActive };
                
                }
                )}
              }
            >
              <i className={`la la-chevron-circle-${isCommentsActive ? 'right' : 'left'}`} />
            </div>
          </div>
          <div className={`home-feeds flex-shrink-1 ${isCommentsActive ? 'active' : ''}`}>
            <div className={`comments shrink-button ${isCommentsActive ? 'hidden' : 'active'}`}>
              <ul>
                <li onClick={() => this.handleCommentView('comments')}>
                  <i className="la la-commenting" />
                </li>
                <li onClick={() => this.handleCommentView('feeds')}>
                  <i className="la la-rss-square" />
                </li>
              </ul>
            </div>
            <div className={`feeds-header ${!isCommentsActive ? 'hidden' : ''}`}>
              <ul>
                {feedMenu.map((i, index) => (
                  <li
                    className={activeFeed === i ? 'active' : ''}
                    onClick={() =>{
                      ReactGA.event(
                        {
                          category : activeFeed ,
                          action : 'User with id ' + this.props.cookies.cookies.userMeta+' is viewing '+activeFeed
                        }
                      ) 
                      this.setState({ activeFeed: i })}}
                    key={index}
                  >
                    {i}
                  </li>
                ))}
              </ul>
              {this.actionButtons(activeFeed)}
            </div>
            <div className={`feeds-container  ${!isCommentsActive ? 'hidden' : ''}`}>
              <ul>
                <li className={` ${activeFeed === 'comments' ? 'active' : 'disabled'}`}>
                  <div className={`feed-comments`}>
                    {topicId && topicId.length > 0 ? (
                      <React.Fragment>
                        {retrieveCategoriesBegin && <Loader />}
                        {comments.data.length > 0 &&
                          comments.data.map((userComment, index) => (
                            <FeedComments
                              cookies={this.props.cookies.cookies}
                              userComment={userComment}
                              key={index}
                              updateRelevance={e => this.setCommentsRelevant(e)}
                            />
                          ))}
                        {!retrieveCategoriesBegin && !comments.data.length && (
                          <div className="no-feed-comment-data">
                            <NoTopic
                              title="No Articles available"
                              message="Please check your Topics and Sources configuration. Otherwise, contact us for assistance."
                              icons={['exclamation-circle']}
                            >
                              <button className="btn-no-data" onClick={() => this.showContactUs()}>
                                CONTACT US
                              </button>
                            </NoTopic>
                          </div>
                        )}
                      </React.Fragment>
                    ) : (
                      <NoTopic
                        title="It’s Empty"
                        message="Add topics and the related comments will appear here."
                        icons={['comments']}
                      >
                        <Link to="/dashboard/topics">
                          <button className="btn-add-topic">ADD TOPIC</button>
                        </Link>
                      </NoTopic>
                    )}
                    {comments['count'] && comments['count'] > comments['data'].length && (
                      <button
                        type="button"
                        className={`btn btn-loadmore ${isCommentsLoading ? 'disabled' : ''}`}
                        onClick={() => this.props.loadMoreComments()}
                      >
                        {isCommentsLoading ? (
                          <React.Fragment>
                            <i className="la la-spinner la-spin" /> Loading
                          </React.Fragment>
                        ) : (
                          'LOAD MORE'
                        )}
                      </button>
                    )}
                  </div>
                </li>
                <li className={` ${activeFeed === 'feeds' ? 'active-feeds' : 'disabled-feeds'}`}>
                  <div className={`feed-comments`}>
                    {topicId && topicId.length > 0 ? (
                      <React.Fragment>
                        {retrieveFeedsBegin && <Loader />}

                        {feeds['data'].length > 0 &&
                          feeds['data'].map((feed, index) => (
                            <Feeds
                              feed={feed}
                              key={index}
                              cookies={this.props.cookies.cookies}
                              updateRelevanceFeed={e => this.setFeedsRelevant(e)}
                              handleFeedDetails={e => this.handleFeedDetails(e, index)}
                            />
                          ))}
                        {!retrieveFeedsBegin && !feeds.data.length && (
                          <div className="no-feed-comment-data">
                            <NoTopic
                              title="No Articles available"
                              message="Please check your Topics and Sources configuration. Otherwise, contact us for assistance."
                              icons={['exclamation-circle']}
                            >
                              <button className="btn-no-data" onClick={() => this.showContactUs()}>
                                CONTACT US
                              </button>
                            </NoTopic>
                          </div>
                        )}
                      </React.Fragment>
                    ) : (
                      <NoTopic
                        title="It’s Empty"
                        message="Add topics and the related comments will appear here."
                        icons={['rss-square']}
                      >
                        <Link to="/dashboard/topics">
                          <button className="btn-add-topic">ADD TOPIC</button>
                        </Link>
                      </NoTopic>
                    )}
                    {feeds['count'] && feeds['count'] > feeds['data'].length && (
                      <button
                        type="button"
                        className={`btn btn-loadmore ${isFeedsLoading ? 'disabled' : ''}`}
                        onClick={() =>{
                          ReactGA.event(
                {
                  category : 'feed' ,
                  action : `more feeds were loaded by ${this.props.cookies.cookies.userMeta} `
                })
                           this.props.loadMoreFeeds()}}
                      >
                        {isFeedsLoading ? (
                          <React.Fragment>
                            <i className="la la-spinner la-spin" /> Loading
                          </React.Fragment>
                        ) : (
                          'LOAD MORE'
                        )}
                      </button>
                    )}
                  </div>
                </li>
              </ul>
            </div>
          </div>
          {isFeedModal && (
            <FeedDetails
              show={isFeedModal}
              toggleFeedDetails={() => this.setState({ isFeedModal: false })}
              feed={feeds['data'] ? feeds['data'][selectedFeedIndex] : []}
              feedDetails={feedDetails}
              updateRelevanceFeed={e => this.setFeedsRelevant(e)}
              loadMoreFeedDetails={() => this.loadMoreFeedDetails()}
              activeFeedCategory={activeFeedCategory}
              retrieveselectedSentmentFeedDetails={e => this.retrieveselectedSentmentFeedDetails(e)}
              updateRelevance={e => this.setCommentsRelevant(e)}
            />
          )}
        </div>
        {isAddChartActive && (
          <AddChartModal
            cookies={this.props.cookies.cookies}
            isAddChartActive={isAddChartActive}
            availableCharts={availableCharts}
            handleAddChart={e => this.setState({ isAddChartActive: e })}
            updateAvailableChart={e => this.updateAvailableChart(e)}
          />
        )}
        {isShareModalActive && (
          <ShareReportModal
            show={isShareModalActive}
            isReportSending={isShareReportSending}
            shareReportStatus={this.props.shareReportStatus}
            sendReportFile={emailList => this.sendReport(emailList)}
            toggleShareReportModal={e => this.setState({ isShareModalActive: e })}
          />
        )}
        {isContactUs && (
          <ContactUs show={isContactUs} toggleModal={e => this.setState({ isContactUs: e })} />
        )}
      </div>
    );
  }
}

class RenderAvailableChart extends Component {
  removeChart = chartId => {
    ReactGA.event(
      {
        category : 'Chart' ,
        action : `user with user id ${this.props.cookies.cookies.userMeta} deleted chart with id ${chartId}`
      }
    )
    this.props.updateSelectedCharts([{ chartId, status: false }]);
  };
  render() {
    const { chart, dateFilterValue, selectedTopicId, source, filterBySentiments } = this.props;
    switch (chart.chartType) {
      case 'areaspline':
        return (
          <div className="chart-content trend">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <AreaChart
              title={chart.name}
              chartId={`chart${chart.chartId}`}
              selectedTopicId={[...selectedTopicId]}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
              filterBySentiments={e => filterBySentiments(e)}
            />
          </div>
        );
      case 'line':
        return (
          <div className="chart-content trend">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <LineChart
              title={chart.name}
              chartId={`chart${chart.chartId}`}
              selectedTopicId={[...selectedTopicId]}
              source={[...source]}
              dateRangeFilterValue={dateFilterValue}
              filterBySentiments={e => filterBySentiments(e)}
            />
          </div>
        );
      case 'donut':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <PieChart
              title={chart.name}
              chartId={`chart${chart.chartId}`}
              selectedTopicId={[...selectedTopicId]}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'spider':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <SpiderChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'bar':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <BarChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'map':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <WorldMapChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'column':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <ColumnChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'sourcespercentcolumn':
        return (
          <div className="chart-content trend">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <SourcesPercentColumnChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'wordcloud':
        return (
          <div className="chart-content">
            <i className="la la-times-circle" onClick={() => this.removeChart(chart.chartId)} />
            <WordCloud
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      case 'sentiments':
        return (
          <div className="chart-emotions">
            <i
              className="la la-times-circle la-close"
              onClick={() => this.removeChart(chart.chartId)}
            />
            <SentimentsChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );

      case 'emotions':
        return (
          <div className="row-emotions">
            <i
              className="la la-times-circle la-close"
              onClick={() => this.removeChart(chart.chartId)}
            />
            <EmotionsChart
              title={chart.name}
              selectedTopicId={[...selectedTopicId]}
              chartId={`chart${chart.chartId}`}
              dateRangeFilterValue={dateFilterValue}
              source={[...source]}
            />
          </div>
        );
      default:
        return <React.Fragment></React.Fragment>;
    }
  }
}

export default Dashboard;
