import React, { Component } from 'react';
import { Modal } from '../../shared-components';
import { Link } from 'react-router-dom';
import { Loader } from '../../shared-components';
import ReactGA from 'react-ga'
class AddCategoryTopic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allCategories: [],
      selectedTopicId: null,
      selectedTopicName: '',
    };
  }

  setActiveTopic = (topIndex, index) => {
    const { allCategories } = this.state;
    const updatedCategories = allCategories.map(category => {
      category.topics.map(topic => {
        topic['topicStatus'] = false;
        return topic;
      });
      return category;
    });

    updatedCategories[topIndex]['topics'][index]['topicStatus'] = !updatedCategories[topIndex][
      'topics'
    ][index]['topicStatus'];
    
    ReactGA.event(
      {
        category : 'Topic' ,
        action : 'Topic with topic_name='+updatedCategories[topIndex]['topics'][index]['topicName']+' of category '+JSON.stringify(updatedCategories[topIndex]['categoryName'])+' was updated'
      }
    )
    this.setState({
      allCategories: updatedCategories,
      selectedTopicId: updatedCategories[topIndex]['topics'][index]['topicId'],
      selectedTopicName: updatedCategories[topIndex]['topics'][index]['topicName'],
    });
  };

  componentDidMount() {
    if (this.props.show) {
      document.body.classList.add('modal-open');
    }
  }

  componentWillUnmount() {
    document.body.classList.remove('modal-open');
  }

  setCategoryId = () => {
    const { selectedTopicId, selectedTopicName } = this.state;
    // this.props.getSelectedTopicId(selectedTopicId);
    ReactGA.event(
      {
        category : 'Topic' ,
        action : `user with id ${this.props.cookies.userMeta} selected Topic ${selectedTopicName}  with Topic_id=${selectedTopicId} to compare`
      }
    )
    this.props.topicSelected({ topicId: selectedTopicId, topicName: selectedTopicName });
    setTimeout(() => {
      this.props.toggleModal(false);
    }, 1000);
  };
  UNSAFE_componentWillReceiveProps(newProps) {
    if (newProps.allCategories.length && !this.state.allCategories.length) {
      this.setState({
        allCategories: newProps.allCategories,
      });
    }
  }

  render() {
    const { toggleModal, show, allCategories } = this.props;

    return (
      <Modal handleClick={() => toggleModal(false)}>
        <div className={`modal add-category-compare ${show ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Select a Category or Topic</h4>

                <i className="la la-close modal-close" onClick={() => toggleModal(false)} />
              </div>

              <div className="modal-body">
                {!allCategories.length && <Loader />}
                {allCategories &&
                  allCategories.map((category, categoryIndex) => (
                    <div className="col-12 list-container" key={categoryIndex}>
                      <div className="topic-list col-12">
                        <ul>
                          <li className="main-list">
                            <label>{category.categoryName}</label>
                          </li>
                          {category.topics.map((topic, index) => (
                            <li
                              className={`sub-list ${topic.topicStatus ? 'active' : ''}`}
                              key={index + 1}
                              onClick={() => this.setActiveTopic(categoryIndex, index)}
                            >
                              <label>{topic.topicName}</label>
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>
                  ))}
              </div>

              <div className="modal-footer">
                <div className="manage-topics">
                  <Link to="/topics">
                    <button className={`btn `}>MANAGE TOPICS</button>
                  </Link>
                  <button className={`btn `} onClick={() => toggleModal(false)}>
                    CANCEL
                  </button>
                  <button
                    className={`btn btn-submit `}
                    onClick={() => {
                      this.setCategoryId();
                    }}
                  >
                    DONE
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

export default AddCategoryTopic;
