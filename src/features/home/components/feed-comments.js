import React, { useState, useEffect, Component } from 'react';
import PropTypes from 'prop-types';
import ReactGA from 'react-ga'

import moment from 'moment';
import { socialIcons } from '../../../styles/icons';

export class FeedComments extends Component {
  static propTypes = {
    userComment: PropTypes.object.isRequired,
    updateRelevance: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      userComment: {
        comment: "",
        source: "",
        sentiment: {
          category: ""
        },
        emotion: {
          category: ""
        }
      },
      updateRelevance: {},
      isSeeMore: false
    };
  }

  componentDidMount() {
    const userComment = this.props.userComment;
    this.setState({
      userComment: userComment
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      userComment: nextProps.userComment,
      updateRelevance: nextProps.updateRelevance
    });
  }

  setRelevance(commentId, currentStatus) {
    const { userComment, updateRelevance } = this.state;
    if (userComment.relevance !== currentStatus) {
      userComment.relevance = currentStatus;
      updateRelevance({ commentId, relevance: currentStatus });
    }
    return false;
  }

  toggleSeeMore = isSeeMore => {
    ReactGA.event(
      {
        category : 'Comments' ,
        action : `User with id ${this.props.cookies.userMeta} was redirected to see more about ${this.props.userComment} from ${this.state.userComment.source}`
      }
    )
    this.setState({isSeeMore});
  }

  _getFeedCommentCSSName = () => {
    const {userComment} = this.state;
    return userComment.source.toLowerCase().replace(/\s+/g, '_');;
  }

  render() {
    const { 
      userComment,
      isSeeMore,
    } = this.state;
    const feedCommentCSSName = this._getFeedCommentCSSName();
    return (
        <div className="comment-card">
          <div className="header">
            <div className="category">
              <div
                className={`social-button card-header-button ${feedCommentCSSName}`}
              >
                <span
                  dangerouslySetInnerHTML={{
                    __html: socialIcons[feedCommentCSSName],
                  }}
                />
              </div>
              <div className="card-header-button">
                {userComment.sentiment.category && (
                  <button className={`btn-positive ${userComment.sentiment.category}`}>
                    {userComment.sentiment.category}
                  </button>
                )}
              </div>
              <div className="card-header-button">
                {userComment.emotion.category !== 'no specific emotion' &&
                  userComment.emotion.category && (
                    <div className="card-header-button">
                      {userComment.emotion.category && (
                        <button className={`btn-emotion ${userComment.emotion.category}`}>
                          {userComment.emotion.category}
                        </button>
                      )}
                    </div>
                  )}
              </div>
            </div>
            <div className="card-header-right w-50">
              <label>{moment(userComment.datetime).format('DD MMM YYYY hh:mm A')}</label>
            </div>
          </div>
          <div className="content">
            <a href={userComment.sourceLink} target="_blank">
              <p className={`${!isSeeMore ? 'shrink-text-view' : ''}`}>{userComment.comment}</p>
            </a>
            {userComment.comment.length > 135 && !isSeeMore ? (
              <span className="see-more-btn" onClick={() => this.toggleSeeMore(!isSeeMore)}>
                <u>See More</u>
              </span>
            ) : (
              userComment.comment.length > 135 &&
              isSeeMore && (
                <span className="see-more-btn less" onClick={() => this.toggleSeeMore(!isSeeMore)}>
                  See Less
                </span>
              )
            )}
          </div>
          <div className="comment-action-btn">
            <button
              className={`btn-relevent ${userComment.relevance ? 'active' : ''}`}
              onClick={() => this.setRelevance(userComment.commentId, true)}
            >
              Relevant
            </button>
            <button
              className={`btn-relevent ${!userComment.relevance ? 'active' : ''}`}
              onClick={() => this.setRelevance(userComment.commentId, false)}
            >
              Irrelevant
            </button>
          </div>
        </div>
      )
  }
};

export default FeedComments;
