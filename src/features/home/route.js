import { DefaultPage } from './';

export default {
  path: 'dashoard',
  name: 'Home',
  childRoutes: [
    { path: '/dashboard', name: 'Default page', component: DefaultPage, isIndex: true },
  ],
};
