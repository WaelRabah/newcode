import { DELETE_REPORTS_BEGIN, DELETE_REPORTS_SUCCESS, DELETE_REPORTS_FAILURE } from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function deleteReport(props = {}) {
  return dispatch => {
    dispatch({
      type: DELETE_REPORTS_BEGIN,
    });
    const url = `${apiEndPoints.reports.DELETE_REPORTS}?reportId=${props.reportId}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.delete(url);
      doRequest.then(
        res => {
          dispatch({
            type: DELETE_REPORTS_SUCCESS,
            data: res.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: DELETE_REPORTS_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case DELETE_REPORTS_BEGIN:
      return {
        ...state,
        deleteReportsBegin: true,
        deleteReportsError: false,
        deleteReportsSuccess: false,
      };
    case DELETE_REPORTS_SUCCESS:
      return {
        ...state,
        deleteReportsBegin: false,
        deleteReportsSuccess: true,
      };
    case DELETE_REPORTS_FAILURE:
      return {
        ...state,
        deleteReportsBegin: false,
        deleteReportsError: true,
      };

    default:
      return state;
  }
}
