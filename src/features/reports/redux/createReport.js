import { CREATE_REPORT_BEGIN, CREATE_REPORT_SUCCESS, CREATE_REPORT_FAILURE } from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';
export function createReport(props = {}) {
  return dispatch => {
    dispatch({
      type: CREATE_REPORT_BEGIN,
    });

    const postData = Object.keys(props).map(params => `${params}=${props[params]}`);
    const url = `${apiEndPoints.reports.CREATE_REPORT}?${postData.join('&')}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.post(url);
      doRequest.then(
        res => {
          if (res.data.status === 200) {
            dispatch({
              type: CREATE_REPORT_SUCCESS,
              data: res.data.data,
            });
            resolve(res);
          } else {
            dispatch({
              type: CREATE_REPORT_FAILURE,
            });
          }
        },
        err => {
          dispatch({
            type: CREATE_REPORT_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case CREATE_REPORT_BEGIN:
      return {
        ...state,
        createReportBegin: true,
        createReportSuccess: false,
        createReportFailure: false,
      };
    case CREATE_REPORT_SUCCESS:
      state.allReports.count = state.allReports.count + 1;
      return {
        ...state,
        createReportBegin: false,
        createReportSuccess: true,
        createdReportId: action.data.reportId,
      };
    case CREATE_REPORT_FAILURE:
      return {
        ...state,
        createReportBegin: false,
        createReportFailure: true,
      };
    default:
      return state;
  }
}
