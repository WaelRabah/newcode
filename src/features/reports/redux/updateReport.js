import { UPDATE_REPORT_BEGIN, UPDATE_REPORT_SUCCESS, UPDATE_REPORT_FAILURE } from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function updateReport(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_REPORT_BEGIN,
    });

    const postData = Object.keys(props).map(params => `${params}=${props[params]}`);
    const url = `${apiEndPoints.reports.UPDATE_REPORT}?${postData.join('&')}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.patch(url);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_REPORT_SUCCESS,
            data: props.reportId,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_REPORT_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case UPDATE_REPORT_BEGIN:
      return {
        ...state,
        updatedReportdBegin: true,
        updatedReportSuccess: false,
        createdReportSuccess: false,
        updatedReportFailure: false,
      };
    case UPDATE_REPORT_SUCCESS:
      return {
        ...state,
        updatedReportBegin: false,
        updatedReportSuccess: true,
        createdReportId: action.data,
      };
    case UPDATE_REPORT_FAILURE:
      return {
        ...state,
        updatedReportBegin: false,
        updatedReportFailure: true,
      };
    default:
      return state;
  }
}
