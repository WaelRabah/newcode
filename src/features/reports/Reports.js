import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { Button, SearchInput, DeleteConfirmation, Loader, NoTopic } from '../shared-components';
import AddReport from './components/AddReport';

import {
  WEEKDAY_INFO_BY_DB_VALUE, 
  UNCATEGORIZED_CATEGORY_NAME, 
  OTHER_CATEGORY_NAME,
  CATEGORIES_TO_PUT_AT_END
} from './redux/constants';
import { GaContext } from '../../googleAnalyticsContext/GaContext';

export class Reports extends Component {
  static propTypes = {
    reports: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
  state = {
    currentPage: 1,
    totalPage: 0,
    pageLimit: 10,
    deleteIndex: -1,
    reportsData: [],
    isAddReport: false,
    addReportActionType: 'add',
    updateReportData: {},
    topicDropdown: [],
    allTopics: []
  };
  addReport = () => {
    window.history.replaceState(this.state,'','/dashboard/reports/addReport')
    
  
    this.setState(prevState => {
      return {
        isAddReport: !prevState.isAddReport,
        addReportActionType: 'add',
      };
    });
  };
  
  removeReport = i => {
    window.history.replaceState(this.state,'','/dashboard/reports/removeReport')
    this.setState({ confirmDeleteModal: true, deleteIndex: i });
  };

  toggleModal = (e) => {
    window.history.replaceState(this.state,'','/dashboard/reports')
    this.setState({ isAddReport: e });
    
  };

  addNewReport = report => {
    const { addReportActionType, updateReportData } = this.state;
    const { createReport, updateReport } = this.props.actions;
    if (addReportActionType === 'add') {
      this.setState({
        updateReportData: report,
      });
      this.context.ReactGA.event({
        category: 'Report',
        action: 'a new report was added report_name='+report.reportName+' by user with id '+this.props.cookies.cookies.userMeta
      });
      createReport(report);
      
    } else {
      const postData = Object.assign({}, report, { reportId: updateReportData.reportId });
      this.setState({
        updateReportData: postData,
      });
      this.context.ReactGA.event({
        category: 'Report',
        action: `report=${report.reportName} was edited by user with id ${this.props.cookies.cookies.userMeta}`
      });
      updateReport(postData);
      
    }
  };

  editReport = report => {
    window.history.replaceState(this.state,'','/dashboard/reports/editReport')
    this.setState(
      {
        addReportActionType: 'edit',
        updateReportData: report,
      },
      () =>
        this.setState({
          isAddReport: true,
        }),
    );
  };

  OrdialSuffix = number => {
    const modTen = number % 10;
    const modHundred = number % 100;
    if (modTen === 1 && modHundred !== 11) {
      return number + 'st';
    }
    if (modTen === 2 && modHundred !== 12) {
      return number + 'nd';
    }
    if (modTen === 3 && modHundred !== 13) {
      return number + 'rd';
    }
    return number + 'th';
  };

  renderFrequency = report => {
    if (report.frequency.unit === 'week') {
      return (
        WEEKDAY_INFO_BY_DB_VALUE[report.frequency.onA].name +
        ' of every ' +
        (report.frequency.every === 1
          ? 'week'
          : report.frequency.every + (report.frequency.every > 1 ? ' weeks' : ' week'))
      );
    } else if (report.frequency.unit === 'month') {
      return (
        this.OrdialSuffix(report.frequency.onA) +
        ' of every ' +
        report.frequency.every +
        (report.frequency.every > 1 ? ' months' : ' month')
      );
    }
    else {
      return (
        'Every ' + report.frequency.every + '  day(s)'
      );
    }
  };
  handlePagination = type => {
    const { currentPage, totalPage } = this.state;
    const { retrieveReports } = this.props.actions;
    switch (type) {
      case 'next':
        if (currentPage < totalPage) {
          this.setState(
            prevState => {
              return { currentPage: prevState.currentPage + 1 };
            },
            () => {
              retrieveReports({ pageId: this.state.currentPage, pageLimit: this.state.pageLimit });
            },
          );
        }
        break;
      case 'prev':
        if (currentPage > 1) {
          this.setState(
            prevState => {
              return { currentPage: prevState.currentPage - 1 };
            },
            () => {
              retrieveReports({ pageId: this.state.currentPage, pageLimit: this.state.pageLimit });
            },
          );
        }
        break;
      default:
        break;
    }
  };
  deleteReport = () => {
    const { deleteReport } = this.props.actions;
    const { deleteIndex, reportsData } = this.state;
    this.context.ReactGA.event({
      category: 'Report',
      action: 'Report with id ='+reportsData[deleteIndex].reportId+`was deleted by ${this.props.cookies.cookies.userMeta}`
    });
    deleteReport({ reportId: reportsData[deleteIndex].reportId });
  };

  PaginationControl = () => {
    const { currentPage, totalPage } = this.state;
    const { allReports } = this.props.reports;
    return (
      <React.Fragment>
        <div className="total-count">
          <label>
            <strong>{allReports['count'] || 0}</strong> Reports
          </label>
        </div>
        <div className="pagination-controls">
          <span className="prev" onClick={() => this.handlePagination('prev')}></span>
          <label>
            Page{' '}
            <strong>
              {currentPage} of {totalPage}
            </strong>
          </label>
          <span className="next" onClick={() => this.handlePagination('next')}></span>
        </div>
      </React.Fragment>
    );
  };
 
  static contextType =GaContext 
  componentDidMount() {

    const {ReactGA} = this.context
    ReactGA.pageview('/dashboard/reports')
   
    const { retrieveReports, retrieveAllTopicsDropdown } = this.props.actions;
    const { currentPage, pageLimit } = this.state;
    retrieveReports({ pageId: currentPage, pageLimit });
    retrieveAllTopicsDropdown();
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (newProps.reports.allReports.data && newProps.reports.allReports.data.length) {
      this.setState({
        reportsData: newProps.reports.allReports.data,
        totalPage: Math.ceil(newProps.reports.allReports.count / this.state.pageLimit),
      });
    }

    if (newProps.reports.deleteReportsSuccess && this.state.deleteIndex > -1) {
      const { retrieveReports } = this.props.actions;
      const { currentPage, pageLimit } = this.state;
      this.setState({ confirmDeleteModal: false, deleteIndex: -1 }, () => {
        retrieveReports({ pageId: currentPage, pageLimit });
      });
    }

    if (newProps.reports.createReportSuccess || newProps.reports.updatedReportSuccess) {
      const { retrieveReports } = this.props.actions;
      let { reportsData, updateReportData, currentPage, pageLimit } = this.state;

      let reportIndex = -1;
      reportsData.forEach((report, index) => {
        if (report.reportId === newProps.reports.createdReportId) {
          reportIndex = index;
        }
      });

      if (
        (newProps.reports.createReportSuccess || newProps.reports.updatedReportSuccess) &&
        Object.keys(updateReportData).length &&
        newProps.reports.createdReportId
      ) {
        reportsData.splice(reportIndex !== -1 ? reportIndex : reportsData.length - 1, 1);
        updateReportData['reportId'] = newProps.reports.createdReportId;
        const frequency = {
          every: updateReportData.frequencyEvery,
          onA: updateReportData.frequencyOnA,
          unit: updateReportData.frequencyUnit,
        };
        updateReportData['frequency'] = frequency;
        updateReportData['recipients'] =
          typeof updateReportData.recipients === 'object'
            ? updateReportData.recipients.join(',').split(',')
            : updateReportData.recipients.split(',');
        updateReportData['topicNames'] =
          typeof updateReportData.topicNames === 'object'
            ? updateReportData.topicNames.join(',').split(',')
            : updateReportData.topicNames.split(',');
        reportIndex !== -1
          ? (reportsData[reportIndex] = updateReportData)
          : reportsData.unshift(updateReportData);

        this.setState({ reportsData, updateReportData: {} }, () => {
          retrieveReports({ pageId: currentPage, pageLimit });
        });
      }
      this.setState({
        isAddReport: false,
      });
    }

    if (
      newProps.reports.retrieveAllTopicsdropdownSuccess &&
      JSON.stringify(newProps.reports.topicDropdown) !== JSON.stringify(this.state.topicDropdown)
    ) {
      this._populateAllTopics(newProps);
    }
  }

  _populateAllTopics(newProps) {
    let {
      categories,
      topics_by_category
    } = this._groupTopicsPropsByCategory(newProps);
    categories.sort();
    let options = this._transformGroupedTopicsToInputFormatForDropDownList(categories,topics_by_category);
    this.setState({
      allTopics: options,
    });

  }
  
  _groupTopicsPropsByCategory(newProps) {
    let topics_by_category = {};
    let categories         = [];
    for (let i=0; i<newProps.reports.topicDropdown.length; i++) {
      let topic = newProps.reports.topicDropdown[i];
      let topicName = topic.topicName[0];
      let category = null;
      if (topic.categories.length > 0) {
        category = topic.categories[0];
      }
      else {
        category = UNCATEGORIZED_CATEGORY_NAME;
      }
      if (! categories.includes(category)) {
        categories.push(category);
      }
      if (! topics_by_category.hasOwnProperty(category)) {
        topics_by_category[category] = [];
      }
      if (! topics_by_category[category].includes(topicName)) {
        topics_by_category[category].push(topicName);
      }
    }
    return {
      categories: categories,
      topics_by_category: topics_by_category
    };
  }

  _transformGroupedTopicsToInputFormatForDropDownList(categories,topics_by_category) {
    let options = [];
    for (let i=0;i<categories.length; i++) {
      let category = categories[i];
      if (CATEGORIES_TO_PUT_AT_END.includes(category)) {
        continue;
      }
      if (topics_by_category[category].length > 0) {
        options.push({
          type:  'group',
          name: category,
          items: topics_by_category[category]
        });
      }
    }

    if (topics_by_category.hasOwnProperty(OTHER_CATEGORY_NAME)) {
      options.push({
        type:  'group',
        name: OTHER_CATEGORY_NAME,
        items: topics_by_category[OTHER_CATEGORY_NAME]
      });
    }
    if (topics_by_category.hasOwnProperty(UNCATEGORIZED_CATEGORY_NAME)) {
      options.push({
        type:  'group',
        name: UNCATEGORIZED_CATEGORY_NAME,
        items: topics_by_category[UNCATEGORIZED_CATEGORY_NAME]
      });
    }
    return options;
  }

  render() {
    
    const {
      confirmDeleteModal,
      deleteIndex,
      isAddReport,
      reportsData,
      addReportActionType,
      updateReportData,
      allTopics,
    } = this.state;
  
    const { deleteReportsBegin, retrieveReportsBegin } = this.props.reports;
    return (
      <div className="reports-container">
        <div className="reports">
          <div className="reports-header">
        
            
          
            <Button label="Add Report" icon="plus" onClick={this.addReport} />
            
            
            <div className="pagination">
              <SearchInput />
              <this.PaginationControl />
            </div>
          </div>

          <div className="report-table">
            {retrieveReportsBegin && <Loader />}
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Report Name</th>
                  <th>Topics</th>
                  <th>Frequency</th>
                  <th>Recipients</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {reportsData.length > 0 &&
                  reportsData.map((report, index) => (
                    <tr key={report.reportId}>
                      <td>{report.reportName}</td>
                      <td>{report.topicNames.join(', ')}</td>
                      <td>{this.renderFrequency(report)}</td>
                      <td>{report.recipients.join(', ')}</td>
                      <td>
                        <button className="btn edit" onClick={() => this.editReport(report)}>
                          Edit
                        </button>
                        <button className="btn delete" onClick={() => this.removeReport(index)}>
                          Delete
                        </button>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
            {!reportsData.length && !retrieveReportsBegin && (
              <NoTopic
                title="Almost There"
                message="Add a report, and start analysing the sentiments and emotions."
                icons={['area-chart', 'pie-chart', 'bar-chart']}
              >
                <button className="btn-add-topic" onClick={this.addReport.bind(this)}>
                  ADD REPORT
                </button>
              </NoTopic>
            )}
          </div>
          <div className="table-footer">
            <div className="pagination">
              <this.PaginationControl />
            </div>
          </div>
        </div>
        {isAddReport && (
          <AddReport
              show={isAddReport}
              toggleModal={this.toggleModal}
              addReportSubmit={e => this.addNewReport(e)}
              type={addReportActionType}
              allTopics={allTopics}
              reportData={updateReportData}
              createReportBeginStatus={this.props.reports.createReportBegin}
            />
         
        )}

        {confirmDeleteModal && (
          <DeleteConfirmation
            show={confirmDeleteModal}
            title="Delete Report"
            disabled={deleteReportsBegin}
            message={
              deleteIndex !== -1
                ? `You are deleting <strong>'${reportsData[deleteIndex].reportName}'</strong>. This action cannot be undone. Proceed?`
                : ''
            }
            toggleModal={()=>{
              window.history.replaceState(this.state,'','/dashboard/reports')
              this.setState({ confirmDeleteModal: false });}}
            confirmDelete={() => this.deleteReport()}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    reports: state.reports,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Reports);
