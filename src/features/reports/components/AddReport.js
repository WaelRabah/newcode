import React, { Component } from 'react';
import { TagsInput, Modal, AutoComplete } from '../../shared-components';
import ReactGA from 'react-ga'
import {WEEKDAYS,
  WEEKDAY_INFO_BY_NAME,
  WEEKDAY_INFO_BY_DB_VALUE,
  WEEKDAY_INFO_BY_JS_VALUE
} from '../redux/constants';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import moment from 'moment';
import {
  REPORT_NAME_INSTRUCTION_MSG,
  MISSING_REPORT_NAME_ERROR_MSG,
  REPORT_DESCRIPTION_INSTRUCTION_MSG,
  MISSING_REPORT_DESCRIPTION_ERROR_MSG,
  REPORT_TOPIC_INSTRUCTION_MSG,
  MISSING_REPORT_TOPIC_ERROR_MSG
} from '../redux/constants';

const frequencyUnit = ['day','week', 'month'];
const emailRegx = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

const monthDays = [
  { day: 1, status: false },
  { day: 2, status: false },
  { day: 3, status: false },
  { day: 4, status: false },
  { day: 5, status: false },
  { day: 6, status: false },
  { day: 7, status: false },
  { day: 8, status: false },
  { day: 9, status: false },
  { day: 10, status: false },
  { day: 11, status: false },
  { day: 12, status: false },
  { day: 13, status: false },
  { day: 14, status: false },
  { day: 15, status: false },
  { day: 16, status: false },
  { day: 17, status: false },
  { day: 18, status: false },
  { day: 19, status: false },
  { day: 20, status: false },
  { day: 21, status: false },
  { day: 22, status: false },
  { day: 23, status: false },
  { day: 24, status: false },
  { day: 25, status: false },
  { day: 26, status: false },
  { day: 27, status: false },
  { day: 28, status: false },
  { day: 29, status: false },
  { day: 30, status: false },
  { day: 31, status: false },
];
const initialState = {
  reportName: '',
  description: '',
  recipients: [],
  topicNames: [],
  frequency: {
    every: 1,
    unit: 'week',
    onA: '',
  },
};
class AddReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: {},
      isFrequencySelectorActive: false,
      reportData: JSON.parse(JSON.stringify(initialState)),
      weekDays: JSON.parse(JSON.stringify(WEEKDAYS)),
      monthDays: JSON.parse(JSON.stringify(monthDays)),
      topicPlaceHolderText: "Select a topic",
      currentDate: moment(),
      firstReportDate: moment(),
      secondReportDate: moment()
    };
    this.frequencyInput = React.createRef();
  }
  validateFrequency = event => {
    if ((event.which != 8 && event.which != 0 && event.which < 48) || event.which > 57) {
      event.preventDefault();
    }
  };
  incrementFrequency = () => {
    let { reportData } = this.state;
    reportData.frequency['every'] = Number(reportData.frequency['every']) + 1;
    this.setState({ reportData }, () => {
      this.frequencyInput.current.value = this.state.reportData.frequency.every;
      this.calculateFirstTwoReportDatesFromBeginningOfYear();
    });
  };
  decrementFrequency = () => {
    let { reportData } = this.state;
    if (reportData.frequency['every'])
      reportData.frequency.every = Number(reportData.frequency.every) - 1;
    this.setState({ reportData }, () => {
      this.frequencyInput.current.value = this.state.reportData.frequency.every;
      this.calculateFirstTwoReportDatesFromBeginningOfYear();
    });
  };

  calculateFirstTwoReportDatesFromBeginningOfYear = () => {
    let { reportData, currentDate } = this.state;
    let firstReportDate = currentDate.clone().startOf('year');
    let secondReportDate = currentDate.clone().startOf('year');
    const unit  = reportData.frequency['unit'];
    const every = reportData.frequency['every'];
    const onA   = reportData.frequency['onA'];
    if (unit === 'month') {
      firstReportDate = currentDate.clone().startOf('year').add(onA - 1,'days');
      secondReportDate = firstReportDate.clone().add(every,'months');
      while (firstReportDate.isBefore(currentDate,'day')) {
        firstReportDate = secondReportDate.clone();
        secondReportDate = firstReportDate.clone().add(every,'months');
      }
    }
    else if (unit === 'week') {
      let weekdayInfo = WEEKDAY_INFO_BY_DB_VALUE[onA];
      firstReportDate  = currentDate.clone().startOf('year');
      if (firstReportDate.day() != weekdayInfo['js_value']) {
        firstReportDate = this._setToNextWeekdayInFuture(firstReportDate, weekdayInfo['name']);
      }
      secondReportDate = this._setToNextWeekdayInterval(firstReportDate, weekdayInfo['name'], every);
      while (firstReportDate.isBefore(currentDate,'day')) {
        firstReportDate = secondReportDate.clone();
        secondReportDate = this._setToNextWeekdayInterval(firstReportDate, weekdayInfo['name'], every);
      }
    }
    else {
      firstReportDate = currentDate.clone().startOf('year');
      if (this._nextReportDateIsInNextYear(firstReportDate,every)) {
        secondReportDate = currentDate.clone().startOf('year').add(1, 'years');
      }
      else {
        secondReportDate = firstReportDate.clone().add((every),'days');
      }
      while (firstReportDate.isBefore(currentDate,'day')) {
        firstReportDate = secondReportDate.clone();
        if (this._nextReportDateIsInNextYear(firstReportDate,every)) {
          secondReportDate = currentDate.clone().startOf('year').add(1, 'years');
        }
        else {
          secondReportDate = firstReportDate.clone().add((every),'days');
        }
      }
    }
    this.setState({firstReportDate, secondReportDate});
  }

  _nextReportDateIsInNextYear(reportDate,every) {
    const nextReportDate = reportDate.clone().add(every,'days');
    if (reportDate.year() < nextReportDate.year()) {
      return true;
    }
    else {
      return false;
    }
  }

  _setToNextWeekdayInFuture(reportDate, dayOfWeekName) {
    let nextWeekDayJSValue = WEEKDAY_INFO_BY_NAME[dayOfWeekName]['js_value'];
    if (reportDate.day() > nextWeekDayJSValue) {
      reportDate = reportDate.clone().day(nextWeekDayJSValue + 7);
    }
    else {
      reportDate = reportDate.clone().day(nextWeekDayJSValue);
    }
    return reportDate;
  }

  _setToNextWeekdayInterval(previousReportDate, dayOfWeekName, every) {
    let { currentDate } = this.state;
    let nextReportDate = previousReportDate.clone().add(every,'weeks');
    if (previousReportDate.year() < nextReportDate.year()) {
      nextReportDate = currentDate.clone().startOf('year').add(1, 'years');
      nextReportDate = this._setToNextWeekdayInFuture(nextReportDate, dayOfWeekName);
    }
    return nextReportDate;
  }

  getFormattedFirstReportDate = () => {
    const {firstReportDate, currentDate } = this.state;
    if (firstReportDate.isSame(currentDate,'day')) {
      return "today within an hour";
    }
    else {
      return "8 am on ".concat(firstReportDate.format('dddd, MMM D, YYYY'));
    }
  }

  getFormattedSecondReportDate = () => {
    const {secondReportDate } = this.state;
    return secondReportDate.format('dddd, MMM D, YYYY');
  }

  setFrequencyType = type => {
    let { reportData } = this.state;
    reportData.frequency['unit'] = type;
    this.setState({ reportData, isFrequencySelectorActive: false }, () => {
      this.setDefaultDay();
      this.calculateFirstTwoReportDatesFromBeginningOfYear();
    });
  };

  setFrequencyWeekDay = newlySelectedWeekDayName => {
    let { weekDays, reportData } = this.state;
    let updatedWeekDays = weekDays.map(day => {
      if (day.name === newlySelectedWeekDayName) {
        day.status = true;
      }
      else {
        day.status = false;
      }
      return day;
    });
    reportData.frequency.onA = WEEKDAY_INFO_BY_NAME[newlySelectedWeekDayName]['db_value'];
    this.setState({
      weekDays: updatedWeekDays,
      reportData 
    }, () => {
      this.calculateFirstTwoReportDatesFromBeginningOfYear();
    });
  };
  setFrequencyMonth = index => {
    let { monthDays, reportData } = this.state;
    let updatedMonth = monthDays.map(day => {
      day.status = false;
      return day;
    });
    updatedMonth[index].status = true;
    reportData.frequency.onA = updatedMonth[index].day;
    this.setState({ monthDays: updatedMonth, reportData }, () => {
      this.calculateFirstTwoReportDatesFromBeginningOfYear();
    });
  };

  setDefaultDay = () => {
    let { reportData } = this.state;
    let type = reportData.frequency['unit'];
    if (type === 'week') {
      this.setFrequencyWeekDay('Monday');
    }
    else if (type === 'month') {
      this.setFrequencyMonth(0);
    }
  }

  addedRecipients = users => {
    let { reportData, error } = this.state;
    error.recipients = false;
    reportData.recipients = users;
    this.setState({ reportData, error });
  };

  addedTopics = topic => {
    let { reportData, error } = this.state;
    error.topicNames = false;
    reportData.topicNames = [topic.value];
    this.setState({ 
      reportData: reportData,
      topicPlaceHolderText: topic.value
    });
  };

  validateEmail = emailError => {
    const { error } = this.state;
    error['recipients'] = !!emailError;
    this.setState({
      error,
    });
  };

  validateForm = form => {
    let result = true;
    let checkEmail = true;
    if (!form.reportName) {
      result = false;
      let { error } = this.state;
      error['reportName'] = true;
      this.setState({ error });
    }
    if (form.description.length == 0) {
      result = false;
      let {error} = this.state;
      error['description'] = true;
      this.setState({error});
    }
    if (!form.recipients.length) {
      result = false;
      let { error } = this.state;
      error['recipients'] = true;
      this.setState({ error });
    }
    if (!form.topicNames.length) {
      result = false;
      let { error } = this.state;
      error['topicNames'] = true;
      this.setState({ error });
    }

    if (form.recipients.length) {
      form.recipients.forEach(user => {
        if (!emailRegx.test(user)) {
          checkEmail = false;
          return false;
        }
      });
      if (!checkEmail) {
        let { error } = this.state;
        error.recipients = true;
        this.setState({ error });
      }
    }
    return result;
  };
  addReportSubmit = () => {
    const { reportData } = this.state;
    
    if (!this.validateForm(reportData)) {
      return false;
    } else {
      const { reportData } = this.state;
      
      const postData = {
        reportName: reportData.reportName,
        topicNames: reportData.topicNames.join(','),
        description: reportData.description,
        frequencyUnit: reportData.frequency.unit,
        frequencyEvery: reportData.frequency.every,
        frequencyOnA: reportData.frequency.onA,
        recipients: reportData.recipients.join(','),
      };
      this.props.addReportSubmit(postData);
    }
  };

  
  componentDidMount() {
    
    
    if (this.props.show) {
      document.body.classList.add('modal-open');
    }
    this.setState({ reportData: JSON.parse(JSON.stringify(initialState)) });
    if (this.props.type === 'add') {
      this.setDefaultDay();
    }
    else if (this.props.type === 'edit') {
      const frequencyType = this.props.reportData.frequency.unit;
      const frequencyValue = this.props.reportData.frequency.onA;
      if (frequencyType.toLowerCase() === 'month') {
        const { monthDays } = this.state;
        const updatedMonth = monthDays.map(month => {
          if (month.day === frequencyValue) {
            month.status = true;
          }
          return month;
        });
        this.setState({ monthDays: updatedMonth });
      } else if (frequencyType === 'week') {
        const { weekDays } = this.state;
        const updatedWeekdays = weekDays.map(day => {
          if (day.db_value === frequencyValue) {
            day.status = true;
          }
          return day;
        });
        this.setState({ weekDays: updatedWeekdays });
      }
      if (this.props.reportData.topicNames.length > 0) {
        this.setState({ topicPlaceHolderText: this.props.reportData.topicNames[0] });
      }
      this.setState({
        reportData: JSON.parse(JSON.stringify(this.props.reportData)),
      });
    }
  }

  componentWillUnmount() {
    this.setState({ reportData: JSON.parse(JSON.stringify(initialState)) });
    document.body.classList.remove('modal-open');
    this.setState({ reportData: {} });
  }

  onChangeForReportNameInput = e => {
    let { reportData, error } = this.state;
    reportData.reportName = e.target.value;
    if (reportData.reportName.length == 0) {
      error.reportName = true;
    }
    else {
      error.reportName = false;
    }
    this.setState({ reportData, error });
  }

  onChangeForDescription = e => {
    let { reportData, error } = this.state;
    reportData.description = e.target.value;
    if (reportData.description.length == 0) {
      error.description = true;
    }
    else {
      error.description = false;
    }
    this.setState({ reportData, error });
  }

  render() {
    const { toggleModal, show, createReportBeginStatus, type, allTopics } = this.props;
 
   if (type==='add')
   {
    ReactGA.pageview('/dashboard/reports/addReport')
   }
   else 
   {
    ReactGA.pageview('/dashboard/reports/editReport')
   }
    const {
      reportData,
      topicPlaceHolderText,
      isFrequencySelectorActive,
      weekDays,
      monthDays,
      error
    } = this.state;
    return (
      <Modal handleClick={() => toggleModal(false)}>
        <div className={`modal add-reports ${show ? 'modal-show' : 'modal-hide'}`}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title"> {type === 'add' ? 'Add Report' : 'Edit Report'}</h4>
                <i className="la la-close modal-close" onClick={() => toggleModal(false)} />
              </div>
              <div className="modal-body">
                <form className="d-flex flex-column pb-0">
                  <div className="form-group d-flex flex-row justify-content-between">
                    <label className="col-form-label">Report Name*</label>
                    <div className="report-input-group">
                      <input
                        type="text"
                        className={`form-control add-report-input ${
                          error && error.reportName ? 'input-error' : ''
                        }`}
                        defaultValue={reportData.reportName}
                        onChange={this.onChangeForReportNameInput}
                        onBlur={this.onChangeForReportNameInput}
                      />
                      {error && error.reportName ? (
                        <span className="instruction-msg error">{MISSING_REPORT_NAME_ERROR_MSG}</span>
                      ) : (
                      <span className="instruction-msg">{REPORT_NAME_INSTRUCTION_MSG}</span>
                      )}
                    </div>
                  </div>
                  <div className="form-group d-flex flex-row justify-content-between">
                    <label className="col-form-label">Description*</label>
                    <div className="report-input-group">
                      <textarea
                        className="form-control add-report-input"
                        maxLength="150"
                        onChange={this.onChangeForDescription}
                        onBlur={this.onChangeForDescription}
                        defaultValue={reportData.description}
                      >
                      </textarea>
                      {error && error.description ? (
                        <span className="instruction-msg error">
                          {MISSING_REPORT_DESCRIPTION_ERROR_MSG}
                        </span>
                      ) : (
                        <span className="instruction-msg">
                          {REPORT_DESCRIPTION_INSTRUCTION_MSG}
                        </span>
                      )}
                    </div>
                  </div>

                  <div className="form-group d-flex flex-row justify-content-between">
                  <label className="col-form-label">Topic*</label>
                  <span className="d-flex flex-column frequency">
                      <Dropdown
                        placeholder={topicPlaceHolderText}
                        options={allTopics}
                        onChange={this.addedTopics}
                      />
                      {error && error.topicNames ? (
                        <div className="instruction-msg error">{MISSING_REPORT_TOPIC_ERROR_MSG}</div>
                      ) : (
                      <div className="instruction-msg">{REPORT_TOPIC_INSTRUCTION_MSG}</div>
                      )}
                  </span>
                </div>
                  <div className="form-group d-flex flex-row justify-content-between">
                    <label className="col-form-label">Frequency*</label>
                    <span className="d-flex flex-column frequency">
                      <span className="d-flex flex-row">
                        <div className="report-input-group add-frequency-container">
                          <label className="">Every</label>
                          <span className="btn-up" onClick={this.incrementFrequency} />
                          <input
                            type="number"
                            ref={this.frequencyInput}
                            defaultValue={reportData.frequency.every}
                            onKeyPress={e => this.validateFrequency(e)}
                            onChange={e => {
                              let { reportData } = this.state;
                              reportData.frequency.every = e.target.value;
                              this.setState({ reportData });
                            }}
                            className="add-frequency"
                            key={reportData.frequency.every}
                          />

                          <span className="btn-down" onClick={this.decrementFrequency} />
                        </div>
                        <div className="report-input-group add-frequency-container dropdown">
                          <input
                            type="checkbox"
                            id="dropDown"
                            className="checkbox-disable"
                            readOnly
                            checked={isFrequencySelectorActive ? true : false}
                          />
                          <label
                            className="add-frequency"
                            htmlFor="dropDown"
                            onClick={() =>
                              this.setState(prevState => {
                                return {
                                  isFrequencySelectorActive: !prevState.isFrequencySelectorActive,
                                };
                              })
                            }
                          >
                            {reportData.frequency.unit}
                          </label>
                          <ul>
                            {frequencyUnit.map((unit, index) => (
                              <li onClick={() => this.setFrequencyType(unit)} key={index}>
                                {unit}
                              </li>
                            ))}
                          </ul>
                        </div>
                      </span>
                      <div className="selector-container d-flex flex-column">
                        <ul className="week-picker">
                          {reportData.frequency.unit === 'week'
                            ? weekDays.map((day, index) => (
                                <li
                                  key={day.name}
                                  className={day.status ? 'active' : ''}
                                  onClick={() => this.setFrequencyWeekDay(day.name)}
                                >
                                  {day.symbol}
                                </li>
                              ))
                            : reportData.frequency.unit === 'month'
                              ? monthDays.map((day, index) => (
                                <li
                                  key={day.day}
                                  className={day.status ? 'active' : ''}
                                  onClick={() => this.setFrequencyMonth(index)}
                                >
                                  {day.day}
                                </li>
                              ))
                            : ( <span></span> )}
                        </ul>
                        <span className="instruction-msg">
                          Note: First report will be sent <strong>{this.getFormattedFirstReportDate()}. Second report on {this.getFormattedSecondReportDate()}, etc</strong>.
                        </span>
                      </div>
                    </span>
                  </div>
                  <div className="form-group d-flex flex-row justify-content-between">
                    <label className="col-form-label">Recipients*</label>
                    <div className="report-input-group">
                      <div
                        className={`add-report-input ${
                          error && error.recipients ? 'input-error' : ''
                        }`}
                      >
                        <TagsInput
                          placeholder="Press comma to add tags"
                          selectedTags={e => this.addedRecipients(e)}
                          tagData={reportData['recipients']}
                          type="email"
                          emailError={e => this.validateEmail(e)}
                        />
                      </div>
                      {error && error.recipients ? (
                        <span className="instruction-msg error">
                          Please enter email address of recipients.
                        </span>
                      ) : (
                        <span className="instruction-msg">
                          Enter an email address and separate them with commas
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="form-group d-flex flex-row justify-content-between mb-0">
                    <label className="col-form-label mandatory-msg">
                      * Denotes mandatory field.
                    </label>
                  </div>
                </form>
              </div>

              <div className="modal-footer">
                <button
                  type="button"
                  className={`btn ${createReportBeginStatus ? 'disabled' : ''}`}
                  onClick={() => toggleModal(false)}
                >
                  CANCEL
                </button>
                <button
                  type="button"
                  className={`btn btn-submit ${createReportBeginStatus ? 'disabled' : ''}`}
                  onClick={() => this.addReportSubmit()}
                >
                  {type === 'add' ? 'ADD' : 'SAVE CHANGES'}
                </button>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

export default AddReport;
