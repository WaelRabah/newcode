import React, { Component } from 'react';
import AbstractChart from './AbstractChart';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { chartColorCode, labelColor } from './colorCodes';
import moment from 'moment';
class BarChart extends AbstractChart {
  constructor(props) {
    super(props);
    this.state = {
      chartData: [],
      source: [],
      mapDataFromProps: [],
      topicId: [],
      filterDateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      mapOptions: {
        chart: {
          type: 'bar',
          backgroundColor: 'white',
          marginBottom: 40,
          marginLeft: 50,
          marginTop: 45,
          width: 370,
          height: 240,
        },
        noData: {
          useHTML: true,
          style: {
            color: '#cccccc',
            fontSize: '14px',
          },
        },
        legend: {
          x: -40,
          y: 80,
          floating: true,
          borderWidth: 1,
          shadow: true,
        },

        plotOptions: {
          series: {
            pointWidth: 10,
          },
          bar: {
            dataLabels: {
              enabled: true,
              color: '#999999',
              style: {
                fontSize: '8px',
                fontWeight: 'normal',
                textOutline: 0,
              },
              y: -2,
            },
          },
        },

        title: {
          text: '',
          align: 'left',
          style: {
            color: '#666666',
            fontWeight: 'bold',
            fontSize: '12px',
          },
        },
        tooltip: { enabled: false },
        credits: {
          enabled: false,
        },
        xAxis: {
          labels: {
            style: {
              color: '#999999',
              fontSize: '8px',
              lineHeight: '5px',
              textTransform: 'capitalize',
            },
            y: 2,
          },
          marginLeft: 50,
        },
        yAxis: {
          title: '',
          labels: {
            style: {
              color: '#999999',
              fontSize: '8px',
              lineHeight: '5px',
              textTransform: 'capitalize',
            },
          },
        },
      },
    };
  }

  parseBarChart = chart => {
    if (Object.keys(chart).length && typeof chart['values'] === 'object') {
      const updatedChart = chart['values'].map((item, index) => {
        return {
          y: item,
          color: chartColorCode[chart.categories[index].toLowerCase()],
          dataLabels: {
            color: labelColor[chart.categories[index].toLowerCase()],
          },
        };
      });
      return {
        categories: chart.categories.map(category => category.replace('_', ' ')),
        data: [
          {
            data: updatedChart,
          },
        ],
      };
    } else {
      return {
        categories: [],
        data: [{}],
      };
    }
  };

  reRenderChartOnChange = props => {
    let { mapOptions } = this.state;
    const { retrieveSelectedChartData } = this.props.actions;
    this.setState(
      {
        mapOptions,
        filterDateRange: props.dateRangeFilterValue,
        source: props.source,
        topicId: props.selectedTopicId,
      },
      () => {
        retrieveSelectedChartData({
          chartId: this.props.chartId.replace('chart', ''),
          dateFilterValue: this.state.filterDateRange,
          topicId: this.state.topicId,
          source: props.source,
        });
      },
    );
  };

  componentDidMount() {
    this.reRenderChartOnChange(this.props);
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { chartId, selectedTopicId } = this.props;
    if (
      JSON.stringify(newProps.BarChart[chartId]) !== JSON.stringify(this.state.mapDataFromProps) &&
      newProps.selectedTopicId != null
    ) {
      this.setState({ chartData: [] });
      const { title } = this.props;
      const chartData = this.parseBarChart(newProps.BarChart[chartId]);
      let { mapOptions } = this.state;
      mapOptions['series'] = chartData['data'];
      mapOptions['xAxis']['categories'] = chartData['categories'];
      mapOptions['title']['text'] = title;
      this.setState({
        chartData,
        mapOptions,
        mapDataFromProps: newProps.BarChart[chartId],
        topicId: newProps.selectedTopicId,
      });
    }

    if (this._shouldReRenderChartBasedOnNewProps(newProps)) {
      this.reRenderChartOnChange(newProps);
    }
  }

  render() {
    const { mapOptions, mapDataFromProps } = this.state;
    return (
      <React.Fragment>
        {Object.keys(mapDataFromProps).length > 0 ? (
          <HighchartsReact
            highcharts={Highcharts}
            options={mapOptions}
            allowChartUpdate={true}
            immutable={false}
            updateArgs={[true, true, true]}
          />
        ) : (
          <HighchartsReact highcharts={Highcharts} options={mapOptions} />
        )}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    BarChart: state.charts,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BarChart);
