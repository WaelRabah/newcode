import React, { Component } from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { worldMap } from './worldMap';
import map from 'highcharts/modules/map';
import noData from 'highcharts/modules/no-data-to-display';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { countryCodeData } from './countryCode';
import moment from 'moment';
noData(Highcharts);
map(Highcharts);
class WorldMapChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      source: [],
      chartData: [],
      topicId: [],
      mapDataFromProps: [],
      filterDateRange: {
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
      },
      mapOptions: {
        chart: {
          width: 370,
          height: 240,
          marginLeft: 10,
        },
        title: {
          text: '',
          align: 'left',
          style: {
            color: '#666666',
            fontWeight: 'bold',
            fontSize: '12px',
          },
        },
        noData: {
          useHTML: true,
          style: {
            color: '#cccccc',
            fontSize: '14px',
          },
        },
        credits: {
          enabled: false,
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          symbolHeight: 50,
          symbolWidth: 10,
          style: {
            color: '#999999',
            fontSize: '8px',
            lineHeight: '5px',
          },
        },
        colorAxis: {
          min: 0,
        },
      },
    };
  }

  reRenderAvailableChart = props => {
    const { retrieveSelectedChartData } = this.props.actions;
    this.setState(
      {
        filterDateRange: props.dateRangeFilterValue,
        source: props.source,
        topicId: props.selectedTopicId,
      },
      () => {
        retrieveSelectedChartData({
          chartId: this.props.chartId.replace('chart', ''),
          dateFilterValue: this.state.filterDateRange,
          topicId: this.state.topicId,
          source: this.state.source,
        });
      },
    );
  };

  componentDidMount() {
    this.reRenderAvailableChart(this.props);
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { chartId, selectedTopicId } = this.props;
    if (
      JSON.stringify(newProps.WorldMap[chartId]) !== JSON.stringify(this.state.mapDataFromProps) &&
      selectedTopicId.length
    ) {
      this.setState({ chartData: [] });
      const { title } = this.props;
      const chartData = newProps.WorldMap[chartId];
      let { mapOptions } = this.state;
      if (chartData.length) {
        const data = chartData.map(map => {
          let colorValue = typeof map[1] === 'string' ? Math.floor(Math.random() * 9) : map[1];
          return [countryCodeData[map[0]], colorValue];
        });
        mapOptions['title']['text'] = title;
        mapOptions['colorAxis']['stops'] =
          typeof chartData[0][1] === 'string'
            ? [
                [0, '#D6ECFE'],
                [0.5, '#7FC4FC'],
                [1, '#0080ED'],
              ]
            : [
                [0, '#D6ECFE'],
                [0.25, '#BCE0FD'],
                [0.5, '#7FC4FC'],
                [0.75, '#2798FB'],
                [1, '#0080ED'],
              ];
        mapOptions['colorAxis']['reversed'] = false;

        const series = [
          {
            mapData: worldMap,
            data,
          },
        ];
        mapOptions['series'] = series;
        this.setState({
          chartData: data,
          mapOptions,
          source: newProps.source,
          mapDataFromProps: newProps.WorldMap[chartId],
          topicId: selectedTopicId,
        });
      }
    }
    if (
      (JSON.stringify(this.state.filterDateRange) !==
        JSON.stringify(newProps.dateRangeFilterValue) ||
        JSON.stringify(newProps.source) !== JSON.stringify(this.state.source) ||
        JSON.stringify(this.state.topicId) !== JSON.stringify(newProps.selectedTopicId)) &&
      selectedTopicId.length
    ) {
      this.reRenderAvailableChart(newProps);
    }
  }

  render() {
    const { mapOptions, mapDataFromProps } = this.state;
    return (
      <div>
        {mapDataFromProps.length > 0 ? (
          <HighchartsReact
            options={mapOptions}
            constructorType={'mapChart'}
            highcharts={Highcharts}
            allowChartUpdate={true}
            immutable={false}
            updateArgs={[true, true, true]}
          />
        ) : (
          <HighchartsReact
            highcharts={Highcharts}
            constructorType={'mapChart'}
            options={mapOptions}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    WorldMap: state.charts,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WorldMapChart);
