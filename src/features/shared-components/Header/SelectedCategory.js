import React, { useState, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';

const SelectedCategory = ({
  topics,
  setSelectedTopicActive,
  isCategorySelectionModalActive,
  topicsDropDown,
}) => {
  const [showCategory, toggleCategory] = useState(false);
  const wrapperRef = useRef(null);
  function useOutsideClick(ref) {
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        toggleCategory(false);
      }
    }
    useEffect(() => {
      document.addEventListener('mousedown', handleClickOutside);
      return () => {
        document.removeEventListener('mousedown', handleClickOutside);
      };
    });
  }

  const handleTopicSelection = index => {
    setSelectedTopicActive({
      topicId: topicsDropDown[index]['topicId'],
      status: !topicsDropDown[index]['status'],
      topicName: topicsDropDown[index]['topicName'][0],
    });
    toggleCategory(!showCategory);
  };
  const handleModal = () => {
    toggleCategory(!showCategory);
    isCategorySelectionModalActive(true);
  };
  useOutsideClick(wrapperRef);
  const activeTopics = topics && Object.keys(topics).length ? topics : {};
  return (
    <React.Fragment>
      <div className={`category-container ${showCategory ? 'active' : ''}`} ref={wrapperRef}>
        <div className="category-title" onClick={() => toggleCategory(!showCategory)}>
          <p>
            {Object.keys(activeTopics).length > 0 ? (
              activeTopics.topicName
            ) : (
              <Link to="/dashboard/topics">
                Add your first topic to start analysing its sentiments
              </Link>
            )}
          </p>
          {topicsDropDown && topicsDropDown.length > 0 ? (
            <i className={`la la-chevron-circle-${showCategory ? 'up' : 'down'} arrow`}></i>
          ) : (
            <Link to="/topics">
              <i className={`la la-plus-${topicsDropDown.length ? '' : 'circle'} `}></i>
            </Link>
          )}
          <div></div>
        </div>
        {topicsDropDown && topicsDropDown.length > 0 ? (
          <div className={`category-list ${showCategory ? 'show' : 'disable'}`}>
            <ul className="main-list">
              {topicsDropDown &&
                topicsDropDown.length > 1 &&
                topicsDropDown.map((topic, index) => (
                  <li
                    className={`${topic.status ? 'active-category' : ''}`}
                    key={index}
                    onClick={() => handleTopicSelection(index)}
                  >
                    <label>{topic.topicName}</label>
                  </li>
                ))}
              {topicsDropDown && topicsDropDown.length < 4 ? (
                <li className="more-topics" style={{'display': 'none'}} onClick={() => toggleCategory(!showCategory)}>
                  <Link to="/topics">
                    <label>Add More Topics</label>
                  </Link>
                </li>
              ) : (
                <li className="more-topics" onClick={handleModal}>
                  <label>More Topics</label>
                </li>
              )}
            </ul>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </React.Fragment>
  );
};

export default SelectedCategory;
