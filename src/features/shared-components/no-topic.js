import React from 'react';

const NoTopic = ({ title, message, icons, children }) => {
  return (
    <div className="text-center content no-topic">
      <div className="icon-group">
        {icons.length > 0 &&
          icons.map((icon, index) => <i className={`la la-${icon}`} key={index} />)}
      </div>
      <h5>{title}</h5>
      <p>{message}</p>
      {children}
    </div>
  );
};

export default NoTopic;
