import React, { Component } from 'react';

const emailRegEx = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
class TagsInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tags: [],
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.tagData !== this.state.tags && nextProps.tagData) {
      this.setState({
        tags: nextProps.tagData,
      });
    }
  }

  removeTags = indexToRemove => {
    const { tags } = this.state;
    const updatedTags = tags.filter((_, index) => index !== indexToRemove);
    this.setState({ tags: updatedTags }, () => this.props.selectedTags(this.state.tags));
  };

  addTags = event => {
    const tag = event.target.value.replace(',', '');
    if (tag === '') {
      if (this.props.type === 'email') {
        this.props.emailError(false);
      }
      return false;
    }
    if (this.props.type === 'email') {
      this.props.emailError(false);
      if (!emailRegEx.test(tag)) {
        this.props.emailError(true);
        return false;
      } else if (this.state.tags.indexOf(tag) !== -1) {
        event.target.value = '';
        return false;
      }
      this.setState(
        prevState => {
          return {
            tags: [...prevState.tags, tag],
          };
        },
        () => this.props.selectedTags(this.state.tags),
      );
    }
    if (tag.length > 1 && (this.props.type !== 'email' || !this.props.type)) {
      this.setState(
        prevState => {
          return {
            tags: [...prevState.tags, tag],
          };
        },
        () => this.props.selectedTags(this.state.tags),
      );
    }
    event.target.value = '';
  };

  validateInput = e => {
    this.addTags(e);
  };
  render() {
    const { placeholder, type } = this.props;
    const { tags } = this.state;
    return (
      <div className="tags-input">
        {tags &&
          tags.map((tag, index) => (
            <span className={`tag-item ${type === 'email' ? 'input-email' : ''}`} key={index}>
              <span>{tag}</span>
              <i className="la la-times-circle-o" onClick={() => this.removeTags(index)} />
            </span>
          ))}
        <input
          type="text"
          onKeyUp={event => (event.key === ',' ? this.addTags(event) : null)}
          placeholder={placeholder}
          onBlur={event => this.validateInput(event)}
        />
      </div>
    );
  }
}

export default TagsInput;
