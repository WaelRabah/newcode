import { Login } from './';
import NewUser from './NewUser';

export default {
  path: 'login',
  name: 'login',
  childRoutes: [{ path: 'login', name: 'Login', component: Login, isIndex: true },
  
  { path: 'newUser', name: 'newUser', component: NewUser, isIndex: true }],
};
