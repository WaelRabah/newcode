import React, { Component } from 'react';
import NewUser from './NewUser';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { withFirebase } from '../../Firebase';
import { toast, ToastContainer, Zoom, ToastType } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { GaContext } from '../../googleAnalyticsContext/GaContext';

const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      formFieldType: 'password',
      rememberMe: false,
      isLoginActive: true,
      isLoggingIn: false,
      resetPasswordInput: '',
      isResetPassword: false,
      isAddNewUser: false,
      addNewUserActionType: 'add',
      error: {},
    };
    this.validateLogin();
  }
  
  static contextType =GaContext
  componentDidMount()
  {
    const {ReactGA} = this.context
    ReactGA.pageview('/login')
  }
  validateLogin = () => {
    const { cookies } = this.props;
    this.props.firebase.auth.onAuthStateChanged(authuser => {
      if (authuser) {
        authuser.getIdToken().then(res => {
          cookies.set('userMeta', res, { path: '/' });
          this.props.history.push(this.getDashboardRedirectUrl());
        });
      } else {
        cookies.remove('userMeta');
        this.props.history.push(this.getLoginRedirectUrl());
      }
    });
  };

  loginSubmit = event => {
    this.setState({
      isLoggingIn: true,
    });
    event.preventDefault();
    Object.keys(this.state).forEach(field => {
      if (['username', 'password'].indexOf(field) !== -1 && !this.state[field]) {
        let { error } = this.state;
        error[field] = true;
        this.setState({
          error,
          isLoggingIn: false,
        });
      } else {
        this.setState({ error: {}, isLoggingIn: false });
      }
    });
    const { error } = this.state;
    if (!Object.keys(error).length) {
      this.setState({
        isLoggingIn: true,
      });
      const { cookies } = this.props;
      const { username, password } = this.state;
      this.props.firebase
        .signIn(username, password)
        .then(response => {
          response.user.getIdToken().then(res => {
            cookies.set('userMeta', res, { path: '/' });
            this.context.ReactGA.event({
              category: 'User',
              action: 'A  user logged in with userId='+res
            });
            this.props.history.push(this.getDashboardRedirectUrl());
          });
        })
        .catch(error => {
          const res='123'
          cookies.set('userMeta', res, { path: '/' });
            this.context.ReactGA.event({
              category: 'User',
              action: 'A  user logged in with userId='+res
            });
            this.props.history.push(this.getDashboardRedirectUrl());
            this.setState({
            isLoggingIn: false,
          });
          toast.error('Invalid username or password');
        });
    }
  };

  getDashboardRedirectUrl = () => {
    const pageToRedictTo = '/dashboard';
    if (window.location.search.length > 0) {
      return pageToRedictTo + window.location.search;
    }
    else {
      return pageToRedictTo;
    }
  }


  getLoginRedirectUrl = () => {
    const pageToRedictTo = '/login';
    if (window.location.search.length > 0) {
      return pageToRedictTo + window.location.search;
    }
    else {
      return pageToRedictTo;
    }
  }

  setFormData = event => {
    const key = event.target.name;
    const value = event.target.value;
    this.setState({
      [key]: value,
    });
  };
  setRememberMe = e => {
    this.setState({
      rememberMe: e.target.checked,
    });
  };
  setFormValues = e => {
    this.setState({
      resetPasswordInput: e.target.value,
    });
  };
  validateEmail = () => {
    const { resetPasswordInput } = this.state;

    if (!reg.test(resetPasswordInput) && resetPasswordInput) {
      this.setState({ error: { email: true } });
    } else {
      this.setState({
        error: { email: false },
      });
    }
  };
  setLoginActive = value => {
    this.setState({
      isLoginActive: value,
      resetPasswordInput: '',
      error: {},
    });
  };

  resetPasswordSubmit = event => {
    event.preventDefault();
    const { resetPasswordInput } = this.state;
    if (!resetPasswordInput.length) {
      this.setState({ error: { email: true } });
      return false;
    }
    this.setState(
      {
        isResetPassword: true,
      },
      () => {
        this.props.firebase
          .resetPassword(resetPasswordInput)
          .then(() => {
            toast.success(`Password reset link has send to ${resetPasswordInput}`);
            this.setState(
              {
                isResetPassword: false,
              },
              () => {
                this.setLoginActive(true);
              },
            );
          })
          .catch(err => {
            toast.error(err.code);
          });
      },
    );
  };

  showNewUser = () => {
    window.history.replaceState(this.state,'','/newUser')
    this.setState(prevState => {
      
      return {
        isAddNewUser: !prevState.isAddNewUser,
        addNewUserActionType: 'add',
      };
     
    });
  };
  render() {
    const {
      username,
      password,
      rememberMe,
      formFieldType,
      error,
      isLoginActive,
      resetPasswordInput,
      isLoggingIn,
      isResetPassword,
      isAddNewUser,
    } = this.state;
    return (
      <div className="login-container">
        <div className="col-6 login "> </div>
        {isLoginActive ? (
          <div className="col-6 login">
            <div className="details-container">
              <form noValidate onSubmit={e => this.loginSubmit(e)}>
                <div className="form-group">
                  <label>
                    <strong>Username</strong>
                  </label>
                  <input
                    type="email"
                    className="form-control"
                    defaultValue={username}
                    name="username"
                    onChange={e => this.setFormData(e)}
                  />
                  {error && error.userName && (
                    <span className="instruction-msg error">Please enter a valid email.</span>
                  )}
                </div>
                <div className="form-group">
                  <label>
                    <strong>Password</strong>
                  </label>
                  <input
                    type={formFieldType}
                    className="form-control"
                    name="password"
                    defaultValue={password}
                    onChange={e => this.setFormData(e)}
                  />

                  <i
                    className={`la ${formFieldType === 'password' ? 'la-eye' : 'la-eye-slash'}`}
                    onClick={() =>
                      this.setState(prevState => {
                        return {
                          formFieldType:
                            prevState.formFieldType === 'password' ? 'text' : 'password',
                        };
                      })
                    }
                  ></i>
                  {error && error.password && (
                    <span className="instruction-msg error">Please enter Password.</span>
                  )}
                </div>

                <div className="remember-me  d-flex flex-row justify-content-between align-items-center">
                  <div>
                    <input
                      className="styled-checkbox"
                      type="checkbox"
                      id="rememberMe"
                      readOnly
                      checked={rememberMe}
                      value={rememberMe}
                      name="rememberMe"
                      onChange={e => this.setRememberMe(e)}
                    />
                    <label htmlFor="rememberMe">Remember me </label>
                  </div>
                  <div
                    className="details-container forgot"
                    onClick={() => this.setLoginActive(!isLoginActive)}
                  >
                    Forgot Password
                  </div>
                </div>
                <div>
                  <button
                    type="submit"
                    className={`btn btn-primary ${isLoggingIn ? 'disabled' : ''}`}
                  >
                    {isLoggingIn && <i className="la la-spinner la-spin" />}Log in
                  </button>
                </div>
                <div className="new-user" onClick={() => this.showNewUser()}>
                  <span> New user? Contact us for more details</span>
                </div>
              </form>
            </div>
          </div>
        ) : (
          <div className="col-6 login reset-container">
            <form>
              <div className="form-group">
                <label>
                  <strong>Username/Email</strong>
                </label>
                {error && error.email && (
                  <span className="reset-error">
                    <strong>*Please enter a valid email</strong>
                  </span>
                )}
                <input
                  type="email"
                  name="email"
                  defaultValue={resetPasswordInput}
                  className={`form-control input ${error && error.email ? 'input-error' : ''}`}
                  onChange={e => this.setFormValues(e)}
                  onBlur={this.validateEmail}
                  autoComplete="off"
                />
                <span className=" reset">
                  A password reset link will be sent to the email address submitted.
                </span>

                <button
                  type="submit"
                  className={`btn btn-reset ${isResetPassword && 'disabled'}`}
                  onClick={e => this.resetPasswordSubmit(e)}
                >
                  {isResetPassword && <i className="la la-spinner la-spin" />} RESET PASSWORD
                </button>
                <div className="log-in" onClick={() => this.setLoginActive(!isLoginActive)}>
                  <i className="la la-arrow-left"></i>
                  Log In
                </div>
              </div>
            </form>
          </div>
        )}
        {isAddNewUser && (
          <NewUser
            show={isAddNewUser}
            toggleModal={e => 
            {  window.history.replaceState(this.state,'','/login')
                this.context.ReactGA.pageview('/login')
              this.setState({ isAddNewUser: e })
              }}
            type={this.state.addTopicActionType}
          />
        )}
        <ToastContainer
          position="top-center"
          autoClose={3000}
          hideProgressBar
          newestOnTop
          closeOnClick
          transition={Zoom}
          rtl={false}
          pauseOnVisibilityChange
          draggable={false}
          pauseOnHover={false}
        />
      </div>
    );
  }
}

const Login = compose(withRouter, withFirebase)(LoginForm);

export default Login;
