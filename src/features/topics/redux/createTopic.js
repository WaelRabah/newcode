import {
  CREATE_TOPICS_BEGIN,
  CREATE_TOPICS_SUCCESS,
  CREATE_TOPICS_FAILURE,
  UPDATE_TOPICS_BEGIN,
  UPDATE_TOPICS_SUCCESS,
  UPDATE_TOPICS_FAILURE,
  RESET_TOPICS_STATE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function createTopics(props = {}) {
  return dispatch => {
    dispatch({
      type: CREATE_TOPICS_BEGIN,
    });
    const postData = Object.keys(props).map(params => `${params}=${props[params]}`);
    const url = `${apiEndPoints.topics.CREATE_TOPICS}?${postData.join('&')}`;

    const promise = new Promise((resolve, reject) => {
      const doRequest = http.post(url);
      doRequest.then(
        res => {
          dispatch({
            type: CREATE_TOPICS_SUCCESS,
            data: res.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: CREATE_TOPICS_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function resetTopicState() {
  return dispatch => {
    dispatch({
      type: RESET_TOPICS_STATE,
    });
  };
}

export function updateTopics(props = {}) {
  return dispatch => {
    dispatch({
      type: UPDATE_TOPICS_BEGIN,
    });
    const postData = Object.keys(props).map(params => `${params}=${props[params]}`);
    const url = `${apiEndPoints.topics.UPDATE_TOPICS}?${postData.join('&')}`;

    const promise = new Promise((resolve, reject) => {
      const doRequest = http.patch(url);
      doRequest.then(
        res => {
          dispatch({
            type: UPDATE_TOPICS_SUCCESS,
            data: props.topicId,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: UPDATE_TOPICS_FAILURE,
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case CREATE_TOPICS_BEGIN:
      return {
        ...state,
        createTopicsBegin: true,
        createTopicsFailure: false,
        createTopicsSuccess: false,
        updateTopicsSuccess: false,
      };
    case CREATE_TOPICS_SUCCESS:
      state.allTopics.count = state.allTopics.count + 1;
      return {
        ...state,
        createTopicsBegin: false,
        createTopicsSuccess: true,
        createdTopicId: action.data.data.topicId,
      };
    case CREATE_TOPICS_FAILURE:
      return {
        ...state,
        createTopicsBegin: false,
        createTopicsFailure: true,
      };
    case UPDATE_TOPICS_BEGIN:
      return {
        ...state,
        updateTopicsBegin: true,
        updateTopicsSuccess: false,
        createTopicsSuccess: false,
      };
    case UPDATE_TOPICS_SUCCESS:
      return {
        ...state,
        updateTopicsSuccess: true,
        updateTopicsBegin: false,
      };
    case UPDATE_TOPICS_FAILURE:
      return {
        ...state,
        updateTopicsFailure: true,
        updateTopicsBegin: false,
      };

    default:
      return state;
  }
}
