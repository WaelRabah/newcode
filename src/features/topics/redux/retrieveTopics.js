import {
  RETRIEVE_TOPICS_BEGIN,
  RETRIEVE_TOPICS_SUCCESS,
  RETRIEVE_TOPICS_FAILURE,
} from './constants';
import http from '../../../common/http';
import { apiEndPoints } from '../../../common/globalConstants';

export function retrieveTopics(props = {}) {
  return dispatch => {
    dispatch({
      type: RETRIEVE_TOPICS_BEGIN,
    });
    const url = `${apiEndPoints.topics.RETRIEVE_TOPICS}?pageLimit=${props.pageLimit}&pageId=${props.pageId}`;
    const promise = new Promise((resolve, reject) => {
      const doRequest = http.get(url);
      doRequest.then(
        res => {
          dispatch({
            type: RETRIEVE_TOPICS_SUCCESS,
            data: res.data,
          });
          resolve(res);
        },
        err => {
          dispatch({
            type: RETRIEVE_TOPICS_FAILURE,
          });
          reject(err);
        },
      );
    });
    return promise;
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case RETRIEVE_TOPICS_BEGIN:
      return {
        ...state,
        retrieveTopicsBegin: true,
        retrieveTopicsFailure: false,
      };
    case RETRIEVE_TOPICS_SUCCESS:
      return {
        ...state,
        retrieveTopicsBegin: false,
        allTopics: action.data,
      };
    case RETRIEVE_TOPICS_FAILURE:
      return {
        retrieveTopicsBegin: false,
        retrieveTopicsFailure: true,
        ...state,
      };

    default:
      return state;
  }
}
