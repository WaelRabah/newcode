const BACKEND_PREFIX = "backend/";
export const apiEndPoints = {
  charts: {
    RETRIEVE_CHARTS: BACKEND_PREFIX + 'charts',
    RETRIEVE_CHART_BY_ID: BACKEND_PREFIX + 'chart?chartId=',
    UPDATE_SELECTED_CHARTS: BACKEND_PREFIX + 'charts',
  },
  topics: {
    RETRIEVE_TOPICS: BACKEND_PREFIX + 'topics',
    CREATE_TOPICS: BACKEND_PREFIX + 'topics',
    UPDATE_TOPICS: BACKEND_PREFIX + 'topics',
    DELETE_TOPIC: BACKEND_PREFIX + 'topics',
    UPDATE_SELECTED_TOPIC: BACKEND_PREFIX + 'topic',
  },
  categories: {
    RETRIEVE_CATEGORIES: BACKEND_PREFIX + 'categories',
  },
  sources: {
    RETRIEVE_SOURCES: BACKEND_PREFIX + 'sources',
    UPDATE_SOURCES: BACKEND_PREFIX + 'sources',
  },
  topicsDropDown: {
    UPDATE_SELECTED_TOPIC: BACKEND_PREFIX + 'topicdropdown',
    RETRIEVE_TOPICDROPDOWN: BACKEND_PREFIX + 'topicdropdown',
    UPDATE_TOPICDROPDOWN: BACKEND_PREFIX + 'topicdropdown',
  },
  comments: {
    RETRIEVE_COMMENTS: BACKEND_PREFIX + 'comments',
    UPDATE_COMMENT_RELEVANT: BACKEND_PREFIX + 'comment',
  },
  feeds: {
    RETRIEVE_FEEDS: BACKEND_PREFIX + 'feeds',
    UPDATE_FEED_RELEVANT: BACKEND_PREFIX + 'feed',
    RETRIEVE_FEED_DETAILS: BACKEND_PREFIX + 'feed',
  },
  shareReport: {
    RETRIEVE_REPORT_STATUS: BACKEND_PREFIX + 'share',
    SEND_SHARE_REPORT: BACKEND_PREFIX + 'share',
  },
  compare: {
    RETRIEVE_COMPARE: BACKEND_PREFIX + 'compare',
  },
  csv: {
    RETRIEVE_CSV: BACKEND_PREFIX + 'csv',
  },
  contact: {
    SEND_MESSAGE: BACKEND_PREFIX + 'contact',
  },
  reports: {
    CREATE_REPORT: BACKEND_PREFIX + 'reports',
    UPDATE_REPORT: BACKEND_PREFIX + 'reports',
    RETRIEVE_REPORTS: BACKEND_PREFIX + 'reports',
    DELETE_REPORTS: BACKEND_PREFIX + 'reports',
  },
  facebook: {
    ROOT_URL: 'https://graph.facebook.com/v6.0/'
  },
  facebook_connection: {
    CHECK_CONNECTION: BACKEND_PREFIX + 'facebook_connection',
    UPDATE_CONNECTION: BACKEND_PREFIX + 'facebook_connection',
    DELETE_CONNECTION: BACKEND_PREFIX + 'facebook_connection'
  }
};
