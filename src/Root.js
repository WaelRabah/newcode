import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import history from './common/history';
import { withCookies } from 'react-cookie';

function renderRouteConfigV3(routes, contextPath, cookies) {
  const children = [];
  const renderRoute = (item, routeContextPath) => {
    let newContextPath;
    
    if (/^\//.test(item.path)) {
      newContextPath = item.path;
    } else {
      newContextPath = `${routeContextPath}/${item.path}`;
    }
    newContextPath = newContextPath.replace(/\/+/g, '/');
    if (item.component && item.childRoutes) {
      const childRoutes = renderRouteConfigV3(item.childRoutes, newContextPath,cookies);
      
      children.push(
        <Route
          key={newContextPath}
          render={props => (
            <item.component {...props} cookies={cookies}>
            
              {childRoutes}
            </item.component>
          )}
          path={newContextPath}
        />,
      );
    } else if (item.component) {
      children.push(
        <Route
        key={newContextPath}
        render={props => (
          <item.component {...props} cookies={cookies} />
          
            
         
        )}
        path={newContextPath}
        exact
      />,
      );
    } else if (item.childRoutes) {
      item.childRoutes.forEach(r => renderRoute(r, newContextPath));
    }
  };

  routes.forEach(item => renderRoute(item, contextPath));

  return <Switch>{children}</Switch>;
}

class Root extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    routeConfig: PropTypes.array.isRequired,
  };

  render() {
    
    
    const children = renderRouteConfigV3(this.props.routeConfig, '/', this.props.cookies);
    return (
      <Provider store={this.props.store}>
        <ConnectedRouter history={history}><React.Fragment cookies={this.props.cookies}>{children}</React.Fragment></ConnectedRouter>
      </Provider>
    );
  }
}
export default withCookies(Root);
