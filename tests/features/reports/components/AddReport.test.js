import React from 'react';
import { shallow } from 'enzyme';
import AddReport  from '../../../../src/features/reports/components/AddReport';
import moment from 'moment';
import {
  SUNDAY,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY,
  WEEKDAY_INFO_BY_NAME
} from '../../../../src/features/reports/redux/constants';

describe('reports/components/AddReport', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<AddReport />);
    expect(renderedComponent.find('.add-reports').length).toBe(1);
  });

  it('calculates the correct report schedule for once-per-month on the first of month when today is Dec 31st', () => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr   = "2019-12-31";
    renderedComponent.setState({currentDate: moment(currentDateStr)});
    renderedComponent.instance().setFrequencyType('month');
    renderedComponent.instance().setFrequencyMonth(0);
    const {
      currentDate,
      firstReportDate,
      secondReportDate
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-01-01");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-02-01");
  });

  it('calculates the correct report schedule for once-per-month on the 1st of month when today is Jan 1st', () => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr   = "2020-01-01";
    renderedComponent.setState({currentDate: moment(currentDateStr)});
    renderedComponent.instance().setFrequencyType('month');
    renderedComponent.instance().setFrequencyMonth(0);
    const {
      currentDate,
      firstReportDate,
      secondReportDate
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-01-01");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-02-01");
  });
  
  it('calculates the correct report schedule for once-per-month on the 2nd of the month when today is Feb 5th.', () => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2020-02-05";
    renderedComponent.setState({currentDate: moment(currentDateStr)});
    renderedComponent.instance().setFrequencyType('month');
    renderedComponent.instance().setFrequencyMonth(1);
    const {
      currentDate,
      firstReportDate,
      secondReportDate,
      reportData,
    } = renderedComponent.state();
    expect(reportData['frequency']['every']).toBe(1);
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-03-02");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-04-02");
  });

  it('calculates the correct report schedule for one-per-two-months on the 4th of the month when today is Feb 5th.', () => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2020-02-05";
    let reportData          = renderedComponent.state()['reportData'];
    reportData.frequency.every = 2;
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.instance().setFrequencyType('month');
    renderedComponent.instance().setFrequencyMonth(3);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-03-04");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-05-04");
  });

  it('calculates the correct report schedule for one-per-two-months on the 4th of the month when today is Dec 5th.', () => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2019-12-05";
    let reportData          = renderedComponent.state()['reportData'];
    reportData.frequency.every = 2;
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.instance().setFrequencyType('month');
    renderedComponent.instance().setFrequencyMonth(3);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-01-04");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-03-04");
  });

  it('calculates the correct next two report dates for one-per-three-months on the 1st of the month when today is April 15th.',() => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2020-04-15";
    let reportData          = renderedComponent.state()['reportData'];
    reportData.frequency.every = 3;
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.instance().setFrequencyType('month');
    renderedComponent.instance().setFrequencyMonth(0);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-07-01");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-10-01");
  });

  it('calculates the correct report schedule for once-per-week on Monday when today is January 1st.',() => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2020-01-01";
    renderedComponent.setState({currentDate: moment(currentDateStr)});
    renderedComponent.instance().setFrequencyType('week');
    renderedComponent.instance().setFrequencyWeekDay(MONDAY);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-01-06");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-01-13");
  });


  it('calculates the correct report schedule for once-per-week on Wednesday when today is January 1st.',() => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2020-01-01";
    renderedComponent.setState({currentDate: moment(currentDateStr)});
    renderedComponent.instance().setFrequencyType('week');
    renderedComponent.instance().setFrequencyWeekDay(WEDNESDAY);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-01-01");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-01-08");
  });


  it('calculates the correct report schedule for once-per-week on Thursday when today is Thursday, Feb 6th.',() => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2020-02-06";
    renderedComponent.setState({currentDate: moment(currentDateStr)});
    renderedComponent.instance().setFrequencyType('week');
    renderedComponent.instance().setFrequencyWeekDay(THURSDAY);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-02-06");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-02-13");
  });


  it('calculates the correct report schedule for once-per-two-weeks on Tuesday when today is Jan 1st.',() => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2020-01-01";
    let reportData          = renderedComponent.state()['reportData'];
    reportData.frequency.every = 2;
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.instance().setFrequencyType('week');
    renderedComponent.instance().setFrequencyWeekDay(TUESDAY);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-01-07");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-01-21");
  });

  it('calculates the correct report schedule for once-per-two-weeks on Tuesday when today is Feb 7th.',() => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2020-02-07";
    let reportData          = renderedComponent.state()['reportData'];
    reportData.frequency.every = 2;
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.instance().setFrequencyType('week');
    renderedComponent.instance().setFrequencyWeekDay(TUESDAY);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-02-18");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-03-03");
  });

  it('calculates the correct report schedule for once-per-two-weeks on Tuesday when today is Dec 30th.',() => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2019-12-30";
    let reportData          = renderedComponent.state()['reportData'];
    reportData.frequency.every = 2;
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.instance().setFrequencyType('week');
    renderedComponent.instance().setFrequencyWeekDay(TUESDAY);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2019-12-31");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-01-07");
  });

  it('calculates the correct report schedule for once-per-two-weeks on Tuesday when today is Dec 31th.',() => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2019-12-31";
    let reportData          = renderedComponent.state()['reportData'];
    reportData.frequency.every = 2;
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.instance().setFrequencyType('week');
    renderedComponent.instance().setFrequencyWeekDay(TUESDAY);

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2019-12-31");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-01-07");
  });

  it('calculates the correct report schedule for once-per-day when today is January 1st.', () => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2020-01-01";
    renderedComponent.setState({currentDate: moment(currentDateStr)});
    renderedComponent.instance().setFrequencyType('day');

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2020-01-01");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-01-02");
  });

  it('calculates the correct report schedule for once-per-day when today is December 31st.', () => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2019-12-31";
    renderedComponent.setState({currentDate: moment(currentDateStr)});
    renderedComponent.instance().setFrequencyType('day');

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2019-12-31");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-01-01");
  });

  it('calculates the correct report schedule for once-per-two-days when today is December 31st.', () => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2019-12-31";
    let reportData          = renderedComponent.state()['reportData'];
    reportData.frequency.every = 2;
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.instance().setFrequencyType('day');

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2019-12-31");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-01-01");
  });

  it('calculates the correct report schedule for once-per-two-days when today is December 30st.', () => {
    const renderedComponent = shallow(<AddReport />);
    const currentDateStr    = "2019-12-30";
    let reportData          = renderedComponent.state()['reportData'];
    reportData.frequency.every = 2;
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.setState({
      currentDate: moment(currentDateStr),
      reportData: reportData
    });
    renderedComponent.instance().setFrequencyType('day');

    const {
      currentDate,
      firstReportDate,
      secondReportDate,
    } = renderedComponent.state();
    expect(currentDate.format('YYYY-MM-DD')).toBe(currentDateStr);
    expect(firstReportDate.format('YYYY-MM-DD')).toBe("2019-12-31");
    expect(secondReportDate.format('YYYY-MM-DD')).toBe("2020-01-01");
  });

  it.skip('Test sunday');
});
