import React from 'react';
import { shallow } from 'enzyme';
import { Reports } from '../../../src/features/reports/Reports';

describe('reports/Reports', () => {
  it('renders node with correct class name', () => {
    const props = {
      reports: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <Reports {...props} />
    );

    expect(
      renderedComponent.find('.reports-container').length
    ).toBe(1);
  });
});
