import React from 'react';
import { shallow } from 'enzyme';
import { Button, SearchInput, DeleteConfirmation } from '../../../../src/features/shared-components';

describe('shared-components/Button', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<Button />);
    expect(renderedComponent.find('.btn-default').length).toBe(1);
  });
});

describe('shared-components/SearchInput', () => {
    it('renders node with correct class name', () => {
      const renderedComponent = shallow(<SearchInput />);
      expect(renderedComponent.find('.search-container').length).toBe(1);
    });
});

describe('shared-components/DeleteConfirmation', () => {
    it('renders node with correct class name', () => {
      const renderedComponent = shallow(<DeleteConfirmation />);
      expect(renderedComponent.find('.delete-confirmation').length).toBe(1);
    });
});