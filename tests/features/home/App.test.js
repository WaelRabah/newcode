import React from 'react';
import { shallow } from 'enzyme';
import { App } from '../../../src/features/home';
import configStore from '../../../src/common/configStore';

describe('home/App', () => {
  it('does a non-empty render (since there is no class name to check for)', () => {
    const store = configStore();
    const renderedComponent = shallow(<App store={store} />);
    expect(renderedComponent.isEmptyRender()).toBe(false);
  });
});
