import React from 'react';
import { shallow } from 'enzyme';
import AddCategoryTopic  from '../../../../src/features/home/components/AddCategoryTopic';

describe('home/components/AddCategoryTopic', () => {
  it('renders node with correct class name', () => {
    const props = {
      toggleModal: false, 
      show: false,
      allCategories: []
    };
    const renderedComponent = shallow(<AddCategoryTopic {...props} />);
    expect(renderedComponent.find('.add-category-compare').length).toBe(1);
  });
});
